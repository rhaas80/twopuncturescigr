

static inline CCTK_REAL Power(CCTK_REAL x, int p)
{
    if (p==2)
        return x*x;
    if (p==3)
        return x*x*x;
    if (p==4)
        return x*x*x*x;
    if (p==5)
        return x*x*x*x*x;
    if (p==6)
        return x*x*x*x*x*x;
    return pow(x, p);
}
static inline CCTK_REAL Sqrt(CCTK_REAL x)
{
    return sqrt(x);
}
/* Jacobi matrix */
static inline CCTK_REAL J11(const CCTK_REAL b,
                            const CCTK_REAL A, const CCTK_REAL B)
{
    return (-32.*(1.+A)*b*B)/((-3.+2.*A+A*A)*(-3.+2.*A+A*A)*(1.+B*B));
}
static inline CCTK_REAL J12(const CCTK_REAL b,
                            const CCTK_REAL A, const CCTK_REAL B)
{
    return (-2.*(5.+A*(2.+A))*b*(-1.+B*B))/((-1.+A)*(3.+A)*(1.+B*B)*(1.+B*B));
}
static inline CCTK_REAL J21(const CCTK_REAL b,
                            const CCTK_REAL A, const CCTK_REAL B)
{
    return (-4.*(5.+A*(2.+A))*b*(-1.+B*B))/
         ((-1.+A)*(-1.+A)*(3.+A)*(3.+A)*(1.+B*B));
}
static inline CCTK_REAL J22(const CCTK_REAL b,
                            const CCTK_REAL A, const CCTK_REAL B)
{
    return (16.*(1.+A)*b*B)/((-3.+2.*A+A*A)*(1.+B*B)*(1.+B*B));
}
/* to be called with i,j from (1,3) */
static inline CCTK_REAL J(int i, int j, const CCTK_REAL b,
                          const CCTK_REAL A, const CCTK_REAL B)
{
    if ((i==3) || (j==3))
        return (i==j)?1.0:0.0;
    if (i==1)
        if (j==1)
            return J11(b, A, B);
        else
            return J12(b, A, B);
    else
        if (j==1)
            return J21(b, A, B);
        else
            return J22(b, A, B);
}
/* first derivatives of Jacobi matrix */
static inline CCTK_REAL J11_1(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
    return (32.*(7.+3.*A*(2.+A))*b*B)/
           ((-1.+A)*(-1.+A)*(-1.+A)*(3.+A)*(3.+A)*(3.+A)*(1.+B*B));
}
static inline CCTK_REAL J11_2(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
    return (32.*(1.+A)*b*(-1+B*B))/
           ((-3.+2*A+A*A)*(-3.+2*A+A*A)*(1.+B*B)*(1.+B*B));
}
static inline CCTK_REAL J12_1(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
    return (32*(1 + A)*b*(-1 + Power(B,2)))/
           (Power(-3 + 2*A + Power(A,2),2)*Power(1 + Power(B,2),2));
}
static inline CCTK_REAL J12_2(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
   return (4*(5 + A*(2 + A))*b*B*(-3 + Power(B,2)))/
          ((-1 + A)*(3 + A)*Power(1 + Power(B,2),3));
}
static inline CCTK_REAL J21_1(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
    return (8*(1 + A)*(13 + A*(2 + A))*b*(-1 + Power(B,2)))/
           (Power(-1 + A,3)*Power(3 + A,3)*(1 + Power(B,2)));
}
static inline CCTK_REAL J21_2(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
    return (-16*(5 + A*(2 + A))*b*B)/
           (Power(-1 + A,2)*Power(3 + A,2)*Power(1 + Power(B,2),2));
}
static inline CCTK_REAL J22_1(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
    return (-16*(5 + A*(2 + A))*b*B)/
           (Power(-1 + A,2)*Power(3 + A,2)*Power(1 + Power(B,2),2));
}
static inline CCTK_REAL J22_2(const CCTK_REAL b,
                              const CCTK_REAL A, const CCTK_REAL B)
{
    return (-16*(1 + A)*b*(-1 + 3*Power(B,2)))/
           ((-3 + 2*A + Power(A,2))*Power(1 + Power(B,2),3));
}
/* to be called with i,j from (1,3) */
static inline CCTK_REAL J_(const int i, const int j, const int k,
                           const CCTK_REAL b,
                           const CCTK_REAL A, const CCTK_REAL B)
{
    if ((i==3) || (j==3))
        return 0.0;
    if (i==1)
        if (j==1)
            if (k==1)
                return J11_1(b, A, B);
            else
                return J11_2(b, A, B);
        else
            if (k==1)
                return J12_1(b, A, B);
            else
                return J12_2(b, A, B);
    else
        if (j==1)
            if (k==1)
                return J21_1(b, A, B);
            else
                return J21_2(b, A, B);
        else
            if (k==1)
                return J22_1(b, A, B);
            else
                return J22_2(b, A, B);
}
static inline CCTK_REAL J11_11(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (-384*(1 + A)*(5 + A*(2 + A))*b*B)/(Power(-1 + A,4)*Power(3 + A,4)*(1 + Power(B,2)));
}
static inline CCTK_REAL J11_12(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (-32*(7 + 3*A*(2 + A))*b*(-1 + Power(B,2)))/
         (Power(-1 + A,3)*Power(3 + A,3)*Power(1 + Power(B,2),2));
}
static inline CCTK_REAL J11_22(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (-64*(1 + A)*b*B*(-3 + Power(B,2)))/
         (Power(-3 + 2*A + Power(A,2),2)*Power(1 + Power(B,2),3));
}
static inline CCTK_REAL J12_11(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (-32*(7 + 3*A*(2 + A))*b*(-1 + Power(B,2)))/
         (Power(-1 + A,3)*Power(3 + A,3)*Power(1 + Power(B,2),2));
}
static inline CCTK_REAL J12_12(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (-64*(1 + A)*b*B*(-3 + Power(B,2)))/
         (Power(-3 + 2*A + Power(A,2),2)*Power(1 + Power(B,2),3));
}
static inline CCTK_REAL J12_22(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (-12*(5 + A*(2 + A))*b*(1 - 6*Power(B,2) + Power(B,4)))/
         ((-1 + A)*(3 + A)*Power(1 + Power(B,2),4));
}
static inline CCTK_REAL J21_11(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (-24*(41 + A*(2 + A)*(26 + A*(2 + A)))*b*(-1 + Power(B,2)))/
         (Power(-1 + A,4)*Power(3 + A,4)*(1 + Power(B,2)));
}
static inline CCTK_REAL J21_12(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (32*(1 + A)*(13 + A*(2 + A))*b*B)/
         (Power(-1 + A,3)*Power(3 + A,3)*Power(1 + Power(B,2),2));
}
static inline CCTK_REAL J21_22(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (16*(5 + A*(2 + A))*b*(-1 + 3*Power(B,2)))/
         (Power(-1 + A,2)*Power(3 + A,2)*Power(1 + Power(B,2),3));
}
static inline CCTK_REAL J22_11(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (32*(1 + A)*(13 + A*(2 + A))*b*B)/
         (Power(-1 + A,3)*Power(3 + A,3)*Power(1 + Power(B,2),2));
}
static inline CCTK_REAL J22_12(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (16*(5 + A*(2 + A))*b*(-1 + 3*Power(B,2)))/
         (Power(-1 + A,2)*Power(3 + A,2)*Power(1 + Power(B,2),3));
}
static inline CCTK_REAL J22_22(const CCTK_REAL b,
                               const CCTK_REAL A, const CCTK_REAL B)
{
    return (192*(1 + A)*b*B*(-1 + Power(B,2)))/
           ((-3 + 2*A + Power(A,2))*Power(1 + Power(B,2),4));
}
/* to be called with i,j,k,l from (1,3) */
static inline CCTK_REAL J__(const int i, const int j, const int k, const int l,
                            const CCTK_REAL b,
                            const CCTK_REAL A, const CCTK_REAL B)
{
    if ((i==3) || (j==3))
        return 0.0;
    if (i==1)
        if (j==1)
            if (k==1)
                if (l==1)
                    return J11_11(b, A, B);
                else
                    return J11_12(b, A, B);
            else
                if (l==1)
                    return J11_12(b, A, B);
                else
                    return J11_22(b, A, B);
        else
            if (k==1)
                if (l==1)
                    return J12_11(b, A, B);
                else
                    return J12_12(b, A, B);
            else
                if (l==1)
                    return J12_12(b, A, B);
                else
                    return J12_22(b, A, B);
    else
        if (j==1)
            if (k==1)
                if (l==1)
                    return J21_11(b, A, B);
                else
                    return J21_12(b, A, B);
            else
                if (l==1)
                    return J21_12(b, A, B);
                else
                    return J21_22(b, A, B);
        else
            if (k==1)
                if (l==1)
                    return J22_11(b, A, B);
                else
                    return J22_12(b, A, B);
            else
                if (l==1)
                    return J22_12(b, A, B);
                else
                    return J22_22(b, A, B);
}

/* inverse Jacobi matrix */
static inline CCTK_REAL Jm11(const CCTK_REAL b,
                             const CCTK_REAL x, const CCTK_REAL r)
{
    /*
    CCTK_REAL aux1, aux2, aux3;
    aux1 = 0.5 * (x*x+r*r)/(b*b) - 0.5;
    aux2 = sqrt(aux1*aux1 + r*r/(b*b));
    aux3 = sqrt(aux1+aux2)+sqrt(aux1+aux2+1.);
    return 2./((1.+aux3)*(1.+aux3))*aux3/sqrt(aux1+aux2)/sqrt(aux1+aux2+1.)*x/
           (b*b)*(1.+aux1/aux2);
    */
    return (8*x*Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
          (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))
         )/Power(x,2))*(Sqrt((Power(r,2) + Power(x,2) + 
           Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))))/Power(b,2)) + Sqrt((Power(r,2) + Power(x,2) + 
           Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))))/Power(b,2))))/
   (Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
     Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
       Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2));
}
static inline CCTK_REAL Jm12(const CCTK_REAL b,
                             const CCTK_REAL x, const CCTK_REAL r)
{
    /*
    CCTK_REAL aux1, aux2, aux3;
    aux1 = 0.5 * (x*x+r*r)/(b*b) - 0.5;
    aux2 = sqrt(aux1*aux1 + r*r/(b*b));
    aux3 = sqrt(aux1+aux2)+sqrt(aux1+aux2+1.);
    return 2./((1.+aux3)*(1.+aux3))*aux3/sqrt(aux1+aux2)/sqrt(aux1+aux2+1.)*r/
           (b*b)*(1.+(1.+aux1)/aux2);
    */
    return (4*r)/(Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
       Power(b,4))*Sqrt((-Power(r,2) + Power(x,2) + 
         Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
              Power(b,4))))/Power(x,2))*(Sqrt(2) + 
       Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
            (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))))/Power(b,2))));
}
static inline CCTK_REAL Jm21(const CCTK_REAL b,
                             const CCTK_REAL x, const CCTK_REAL r)
{
    /*
    CCTK_REAL aux1, aux2, T, aux4;
    aux1 = 0.5 * (x*x+r*r)/(b*b) - 0.5;
    aux2 = sqrt(aux1*aux1 + r*r/(b*b));
    T    = -aux1 + aux2;
    aux4 = sqrt(T)/(1.+sqrt(1.-T));
    return x/(b*b)/(1.+aux4)/(1.+aux4)*(aux1/aux2-1.)*
           (aux4/T/sqrt(1.-T));
    */
    return
    (-2*Sqrt(2)*x*(-Power(r,2) - Power(x,2) + Power(b,2)*
        (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
     (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
   (Power(b,4)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
     Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) + Sqrt((-2*(Power(r,2) + Power(x,2)) + 
           2*Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))))/Power(b,2)),2)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
         Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/Power(b,4)));
}
static inline CCTK_REAL Jm22(const CCTK_REAL b,
                             const CCTK_REAL x, const CCTK_REAL r)
{
    /*
    CCTK_REAL aux1, aux2, T, aux4;
    aux1 = 0.5 * (x*x+r*r)/(b*b) - 0.5;
    aux2 = sqrt(aux1*aux1 + r*r/(b*b));
    T    = -aux1 + aux2;
    aux4 = sqrt(T)/(1.+sqrt(1.-T));
    return r/(b*b)/(1.+aux4)/(1.+aux4)*((aux1+1.)/aux2-1.)*aux4/T/sqrt(1.-T);
    */
    return
    (-2*Sqrt(2)*r*(-Power(r,2) - Power(x,2) + Power(b,2)*
        (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
     (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
   (Power(b,4)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
     Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) + Sqrt((-2*(Power(r,2) + Power(x,2)) + 
           2*Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))))/Power(b,2)),2)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
         Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/Power(b,4)));
}
/* to be called with i,j from (1,3) */
static inline CCTK_REAL Jm(int i, int j, const CCTK_REAL b,
                           const CCTK_REAL x, const CCTK_REAL r)
{
    if ((i==3) || (j==3))
        return (i==j)?1.0:0.0;
    if (i==1)
        if (j==1)
            return Jm11(b, x, r);
        else
            return Jm12(b, x, r);
    else
        if (j==1)
            return (x<0)?-Jm21(b, x, r):Jm21(b, x, r);
        else
            return (x<0)?-Jm22(b, x, r):Jm22(b, x, r);
}
static inline CCTK_REAL Jm11_1(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    (8*Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/
        Power(x,2))*(Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
    (Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)),2)) + 
   (4*x*(2*x + (2*x*(-Power(b,2) + Power(r,2) + Power(x,2)))/
         (Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*(1/
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + 1/
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2))))/
    (Power(b,4)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)),2)) - 
   (16*Sqrt(2)*Power(x,2)*Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(x,2))*
      Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/(Power(r,2) + Power(x,2) + Power(b,2)*
           (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))*
      Power(Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)),3)) - 
   (16*Power(x,2)*(-Power(b,2) + Power(r,2) + Power(x,2))*Sqrt((-Power(r,2) + Power(x,2) + 
          Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(x,2))*
      (Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) + Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,6)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),1.5)*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)),2)) + 
   (8*(-(Power(r,2)*(Power(r,2) + Power(x,2))) + Power(b,4)*(-1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
        Power(b,2)*(Power(x,2) + Power(r,2)*(-2 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4)))))*(Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
    (Power(x,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)),2));
}

static inline CCTK_REAL Jm11_2(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    (4*x*(2*r + (2*r*(Power(b,2) + Power(r,2) + Power(x,2)))/
         (Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))))*Sqrt((-Power(r,2) + Power(x,2) + 
          Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*(1/
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + 1/
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2))))/
    (Power(b,4)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/
           Power(b,2)),2)) - (16*Sqrt(2)*r*x*Sqrt((-Power(r,2) + Power(x,2) + 
          Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*Power(Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/
        (Power(r,2) + Power(x,2) + Power(b,2)*(1 + 
             Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)),3)) + 
   (8*r*(Power(r,2) + Power(x,2) - Power(b,2)*(-1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) + Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
    (x*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*Power(2 + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)),2)) - 
   (16*r*x*(Power(b,2) + Power(r,2) + Power(x,2))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*(Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
    (Power(b,6)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),
       1.5)*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)),2));
}

static inline CCTK_REAL Jm12_1(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    (4*x*(2*r + (2*r*(Power(b,2) + Power(r,2) + Power(x,2)))/
         (Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))))*Sqrt((-Power(r,2) + Power(x,2) + 
          Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*(1/
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + 1/
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2))))/
    (Power(b,4)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/
           Power(b,2)),2)) - (16*Sqrt(2)*r*x*Sqrt((-Power(r,2) + Power(x,2) + 
          Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*Power(Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/
        (Power(r,2) + Power(x,2) + Power(b,2)*(1 + 
             Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)),3)) + 
   (8*r*(Power(r,2) + Power(x,2) - Power(b,2)*(-1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) + Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
    (x*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*Power(2 + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)),2)) - 
   (16*r*x*(Power(b,2) + Power(r,2) + Power(x,2))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*(Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                   Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))))/
    (Power(b,6)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),
       1.5)*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
              (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)),2));
}

static inline CCTK_REAL Jm12_2(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    4/(Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/
        Power(x,2))*(Sqrt(2) + Sqrt((Power(r,2) + Power(x,2) + 
            Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)))) - (4*Power(r,2)*
      Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
           (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))
          )/Power(b,2)))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*Power(Sqrt(2) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2)) - 
   (4*Power(r,2)*(Power(r,2) + Power(x,2) - Power(b,2)*
         (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))
      )/(Power(x,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Power((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2),1.5)*(Sqrt(2) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)))) - (8*Power(r,2)*(Power(b,2) + Power(r,2) + Power(x,2)))/
    (Power(b,6)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),
       1.5)*Sqrt((-Power(r,2) + Power(x,2) + Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(x,2))*(Sqrt(2) + 
        Sqrt((Power(r,2) + Power(x,2) + Power(b,2)*
             (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2))));
}

static inline CCTK_REAL Jm21_1(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    (-2*Sqrt(2)*(-Power(r,2) - Power(x,2) + Power(b,2)*
         (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2))))/
    (Power(b,4)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt((-2*(Power(r,2) + Power(x,2)) + 
            2*Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4))) - (4*Power(x,2)*
      Power(Power(r,2) + Power(x,2) - Power(b,2)*
         (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))),
       2))/(Power(b,4)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(b,2))*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4))*Power(2 + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt(-((Power(r,2) + Power(x,2) - Power(b,2)*
                (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                    Power(b,4))))/Power(b,2))),2)) + 
   (4*Sqrt(2)*Power(x,2)*(-Power(r,2) - Power(x,2) + 
        Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))))*(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)) - 
   (4*Sqrt(2)*Power(x,2)*(Power(b,2) - Power(r,2) - Power(x,2))*
      (-Power(r,2) - Power(x,2) + Power(b,2)*(1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,8)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),
       1.5)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4))*Power(2 + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt(-((Power(r,2) + Power(x,2) - Power(b,2)*
                (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                    Power(b,4))))/Power(b,2))),2)) - 
   (4*Sqrt(2)*Power(x,2)*Power(Power(r,2) + Power(x,2) - 
        Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))),2)*(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))*
      (Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
             (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) - Sqrt(-((Power(r,2) + Power(x,2) - 
              Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                     Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),3)*
      (-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
         (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                  Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))) + 
   (2*Sqrt(2)*Power(x,2)*(-Power(r,2) - Power(x,2) + 
        Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
           Power(b,4)))*Power(Power(r,2) + Power(x,2) - 
        Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))),2)*(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)*
      (-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
         (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                  Power(Power(r,2) + Power(x,2),2))/Power(b,4))))));
}

static inline CCTK_REAL Jm21_2(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    (-4*r*x*Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(b,2))*(Power(r,2) + Power(x,2) - 
        Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4)))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)) - 
   (4*Sqrt(2)*r*x*(Power(r,2) + Power(x,2) - Power(b,2)*
         (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))
       *(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)) + 
   (4*Sqrt(2)*r*x*(Power(b,2) + Power(r,2) + Power(x,2))*
      (-Power(r,2) - Power(x,2) + Power(b,2)*(1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,8)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),
       1.5)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4))*Power(2 + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt(-((Power(r,2) + Power(x,2) - Power(b,2)*
                (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                    Power(b,4))))/Power(b,2))),2)) - 
   (4*Sqrt(2)*r*x*(-Power(r,2) - Power(x,2) + Power(b,2)*
         (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))
       *(-Power(r,2) - Power(x,2) + Power(b,2)*(1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))*
      (Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
             (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) - Sqrt(-((Power(r,2) + Power(x,2) - 
              Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                     Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),3)*
      (-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
         (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                  Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))) + 
   (2*Sqrt(2)*r*x*(-Power(r,2) - Power(x,2) + Power(b,2)*
         (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)))*(2*Power(Power(r,2) + Power(x,2),2) - 
        Power(b,4)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))) - Power(b,2)*(Power(r,2)*
            (-3 + 2*Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))) + Power(x,2)*(1 + 2*
               Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))
        ))/(Power(b,10)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt((-2*(Power(r,2) + Power(x,2)) + 
            2*Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2)*Power((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4),1.5));
}

static inline CCTK_REAL Jm22_1(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    (-4*r*x*Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(b,2))*(Power(r,2) + Power(x,2) - 
        Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4)))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)) + 
   (4*Sqrt(2)*r*x*(-Power(b,2) + Power(r,2) + Power(x,2))*
      (-Power(r,2) - Power(x,2) + Power(b,2)*(-1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,8)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),
       1.5)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4))*Power(2 + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt(-((Power(r,2) + Power(x,2) - Power(b,2)*
                (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                    Power(b,4))))/Power(b,2))),2)) + 
   (4*Sqrt(2)*r*x*(-Power(r,2) - Power(x,2) + Power(b,2)*
         (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)) - 
   (4*Sqrt(2)*r*x*(-Power(r,2) - Power(x,2) + Power(b,2)*
         (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))
       *(-Power(r,2) - Power(x,2) + Power(b,2)*(1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))*
      (Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
             (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) - Sqrt(-((Power(r,2) + Power(x,2) - 
              Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                     Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),3)*
      (-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
         (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                  Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))) + 
   (2*Sqrt(2)*r*x*(-Power(r,2) - Power(x,2) + Power(b,2)*
         (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)))*(2*Power(Power(r,2) + Power(x,2),2) + 
        Power(b,4)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))) + Power(b,2)*(Power(r,2)*
            (1 - 2*Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))) - Power(x,2)*(3 + 2*
               Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))
        ))/(Power(b,10)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt((-2*(Power(r,2) + Power(x,2)) + 
            2*Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2)*Power((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4),1.5));
}

static inline CCTK_REAL Jm22_2(const CCTK_REAL b,
                               const CCTK_REAL x, const CCTK_REAL r)
{
    return
    (-2*Sqrt(2)*(-Power(r,2) - Power(x,2) + Power(b,2)*
         (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2))))/
    (Power(b,4)*Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt((-2*(Power(r,2) + Power(x,2)) + 
            2*Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)),2)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4))) - (4*Power(r,2)*
      Power((Power(r,2) + Power(x,2) - Power(b,2)*
           (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
               Power(b,4))))/Power(b,2),1.5))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)) + 
   (4*Sqrt(2)*Power(r,2)*(-Power(r,2) - Power(x,2) + 
        Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))))*(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)) + 
   (4*Sqrt(2)*Power(r,2)*(Power(b,2) + Power(r,2) + Power(x,2))*
      (-Power(r,2) - Power(x,2) + Power(b,2)*(-1 + 
           Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4))))*
      (2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,8)*Power((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/Power(b,4),
       1.5)*Sqrt((-Power(Power(r,2) + Power(x,2),2) + 
          Power(b,2)*(Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))) + 
             Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4)))))/Power(b,4))*Power(2 + 
        Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
              (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))))/Power(b,2)) + Sqrt(2)*
         Sqrt(-((Power(r,2) + Power(x,2) - Power(b,2)*
                (1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                    Power(b,4))))/Power(b,2))),2)) - 
   (4*Sqrt(2)*Power(r,2)*Power(Power(r,2) + Power(x,2) - 
        Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))),2)*(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))*
      (Sqrt((Power(r,2) + Power(x,2) - Power(b,2)*
             (-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                 Power(b,4))))/Power(b,2)) - Sqrt(-((Power(r,2) + Power(x,2) - 
              Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                     Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)))))/
    ((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),3)*
      (-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
         (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                  Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))) + 
   (2*Sqrt(2)*Power(r,2)*(-Power(r,2) - Power(x,2) + 
        Power(b,2)*Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
           Power(b,4)))*Power(Power(r,2) + Power(x,2) - 
        Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
             Power(b,4))),2)*(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))))/
    (Power(b,2)*(Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))*
      Sqrt((-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
           (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/
                  Power(b,4))) + Power(x,2)*(1 + 
                Sqrt((Power(b,4) + 2*Power(b,2)*(r - x)*(r + x) + Power(Power(r,2) + Power(x,2),2))/Power(b,4)))))/
        Power(b,4))*Power(2 + Sqrt(2)*Sqrt((Power(r,2) + Power(x,2) - 
             Power(b,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                    Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2)) + 
        Sqrt(2)*Sqrt(-((Power(r,2) + Power(x,2) - 
               Power(b,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                      Power(Power(r,2) + Power(x,2),2))/Power(b,4))))/Power(b,2))),2)*
      (-Power(Power(r,2) + Power(x,2),2) + Power(b,2)*
         (Power(r,2)*(-1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + Power(Power(r,2) + Power(x,2),2))/
                Power(b,4))) + Power(x,2)*(1 + Sqrt((Power(b,4) + 2*Power(b,2)*(Power(r,2) - Power(x,2)) + 
                  Power(Power(r,2) + Power(x,2),2))/Power(b,4))))));
}

static inline CCTK_REAL Jm_(const int i, const int j, const int k,
                            const CCTK_REAL b,
                            const CCTK_REAL x, const CCTK_REAL r)
{
    if ((i==3) || (j==3))
        return 0.0;
    if (i==1)
        if (j==1)
            if (k==1)
                return Jm11_1(b, x, r);
            else
                return Jm11_2(b, x, r);
        else
            if (k==1)
                return Jm12_1(b, x, r);
            else
                return Jm12_2(b, x, r);
    else
        if (j==1)
            if (k==1)
                return (x<0)?-Jm21_1(b, x, r):Jm21_1(b, x, r);
            else
                return (x<0)?-Jm21_2(b, x, r):Jm21_2(b, x, r);
        else
            if (k==1)
                return (x<0)?-Jm22_1(b, x, r):Jm22_1(b, x, r);
            else
                return (x<0)?-Jm22_2(b, x, r):Jm22_2(b, x, r);
}

static inline CCTK_REAL *get_U_(const derivs U, const int i)
{
    if (i==1)
        return U.d1;
    else if (i==2)
        return U.d2;
    else
        return U.d3;
}
static inline CCTK_REAL *get_U__(const derivs U, const int i, const int j)
{
    if (i==1)
        if (j==1)
            return U.d11;
        else if (j==2)
            return U.d12;
        else
            return U.d13;
    else if (i==2)
        if (j==1)
            return U.d12;
        else if (j==2)
            return U.d22;
        else
            return U.d23;
    else
        if (j==1)
            return U.d13;
        else if (j==2)
            return U.d23;
        else
            return U.d33;
}
