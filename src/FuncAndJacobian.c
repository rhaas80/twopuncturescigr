/* TwoPunctures:  File  "FuncAndJacobian.c"*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <time.h>
#include "cctk.h"
#include "cctk_Parameters.h"
#include "TP_utilities.h"
#include "TwoPunctures.h"

#define FAC sin(al)*sin(be)*sin(al)*sin(be)*sin(al)*sin(be)
/*#define FAC sin(al)*sin(be)*sin(al)*sin(be)*/
/*#define FAC 1*/


/* --------------------------------------------------------------------------*/
int
Index (int ivar, int i, int j, int k, int nvar, int n1, int n2, int n3)
{
  int i1 = i, j1 = j, k1 = k;

  if (i1 < 0)
    i1 = -(i1 + 1);
  if (i1 >= n1)
    i1 = 2 * n1 - (i1 + 1);

  if (j1 < 0)
    j1 = -(j1 + 1);
  if (j1 >= n2)
    j1 = 2 * n2 - (j1 + 1);

  if (k1 < 0)
    k1 = k1 + n3;
  if (k1 >= n3)
    k1 = k1 - n3;

  return ivar + nvar * (i1 + n1 * (j1 + n2 * k1));
}

/* --------------------------------------------------------------------------*/
void
allocate_derivs (derivs * v, int n)
{
  int m = n - 1;
  (*v).d0 = dvector (0, m);
  (*v).d1 = dvector (0, m);
  (*v).d2 = dvector (0, m);
  (*v).d3 = dvector (0, m);
  (*v).d11 = dvector (0, m);
  (*v).d12 = dvector (0, m);
  (*v).d13 = dvector (0, m);
  (*v).d22 = dvector (0, m);
  (*v).d23 = dvector (0, m);
  (*v).d33 = dvector (0, m);
}

/* --------------------------------------------------------------------------*/
void
free_derivs (derivs * v, int n)
{
  int m = n - 1;
  free_dvector ((*v).d0, 0, m);
  free_dvector ((*v).d1, 0, m);
  free_dvector ((*v).d2, 0, m);
  free_dvector ((*v).d3, 0, m);
  free_dvector ((*v).d11, 0, m);
  free_dvector ((*v).d12, 0, m);
  free_dvector ((*v).d13, 0, m);
  free_dvector ((*v).d22, 0, m);
  free_dvector ((*v).d23, 0, m);
  free_dvector ((*v).d33, 0, m);
}

/* --------------------------------------------------------------------------*/
/* Calculates the derivatives of v.d0 (v.d? and v.d??) with resprect to
   A, B and phi */
void
Derivatives_AB3 (int nvar, int n1, int n2, int n3, derivs v)
{
  int i, j, k, ivar, N, *indx;
  CCTK_REAL *p, *dp, *d2p, *q, *dq, *r, *dr;

  N = maximum3 (n1, n2, n3);
  p = dvector (0, N);
  dp = dvector (0, N);
  d2p = dvector (0, N);
  q = dvector (0, N);
  dq = dvector (0, N);
  r = dvector (0, N);
  dr = dvector (0, N);
  indx = ivector (0, N);

  for (ivar = 0; ivar < nvar; ivar++)
  {
    for (k = 0; k < n3; k++)
    {				/* Calculation of Derivatives w.r.t. A-Dir. */
      for (j = 0; j < n2; j++)
      {				/* (Chebyshev_Zeros)*/
	for (i = 0; i < n1; i++)
	{
	  indx[i] = Index (ivar, i, j, k, nvar, n1, n2, n3);
	  p[i] = v.d0[indx[i]];
	}
	chebft_Zeros (p, n1, 0);
	chder (p, dp, n1);
	chder (dp, d2p, n1);
	chebft_Zeros (dp, n1, 1);
	chebft_Zeros (d2p, n1, 1);
	for (i = 0; i < n1; i++)
	{
	  v.d1[indx[i]] = dp[i];
	  v.d11[indx[i]] = d2p[i];
	}
      }
    }
    for (k = 0; k < n3; k++)
    {				/* Calculation of Derivatives w.r.t. B-Dir. */
      for (i = 0; i < n1; i++)
      {				/* (Chebyshev_Zeros)*/
	for (j = 0; j < n2; j++)
	{
	  indx[j] = Index (ivar, i, j, k, nvar, n1, n2, n3);
	  p[j] = v.d0[indx[j]];
	  q[j] = v.d1[indx[j]];
	}
	chebft_Zeros (p, n2, 0);
	chebft_Zeros (q, n2, 0);
	chder (p, dp, n2);
	chder (dp, d2p, n2);
	chder (q, dq, n2);
	chebft_Zeros (dp, n2, 1);
	chebft_Zeros (d2p, n2, 1);
	chebft_Zeros (dq, n2, 1);
	for (j = 0; j < n2; j++)
	{
	  v.d2[indx[j]] = dp[j];
	  v.d22[indx[j]] = d2p[j];
	  v.d12[indx[j]] = dq[j];
	}
      }
    }
    for (i = 0; i < n1; i++)
    {				/* Calculation of Derivatives w.r.t. phi-Dir. (Fourier)*/
      for (j = 0; j < n2; j++)
      {
	for (k = 0; k < n3; k++)
	{
	  indx[k] = Index (ivar, i, j, k, nvar, n1, n2, n3);
	  p[k] = v.d0[indx[k]];
	  q[k] = v.d1[indx[k]];
	  r[k] = v.d2[indx[k]];
	}
	fourft (p, n3, 0);
	fourder (p, dp, n3);
	fourder2 (p, d2p, n3);
	fourft (dp, n3, 1);
	fourft (d2p, n3, 1);
	fourft (q, n3, 0);
	fourder (q, dq, n3);
	fourft (dq, n3, 1);
	fourft (r, n3, 0);
	fourder (r, dr, n3);
	fourft (dr, n3, 1);
	for (k = 0; k < n3; k++)
	{
	  v.d3[indx[k]] = dp[k];
	  v.d33[indx[k]] = d2p[k];
	  v.d13[indx[k]] = dq[k];
	  v.d23[indx[k]] = dr[k];
	}
      }
    }
  }
  free_dvector (p, 0, N);
  free_dvector (dp, 0, N);
  free_dvector (d2p, 0, N);
  free_dvector (q, 0, N);
  free_dvector (dq, 0, N);
  free_dvector (r, 0, N);
  free_dvector (dr, 0, N);
  free_ivector (indx, 0, N);
}

/* --------------------------------------------------------------------------*/
void
F_of_v (CCTK_POINTER_TO_CONST cctkGH,
        int nvar, int n1, int n2, int n3, derivs v, CCTK_REAL *F,
        derivs u, CCTK_REAL *sources)
{
  /*      Calculates the left hand sides of the non-linear equations F_m(v_n)=0*/
  /*      and the function u (u.d0[]) as well as its derivatives*/
  /*      (u.d1[], u.d2[], u.d3[], u.d11[], u.d12[], u.d13[], u.d22[], u.d23[], u.d33[])*/
  /*      at interior points and at the boundaries "+/-"*/
  DECLARE_CCTK_PARAMETERS;
  int i, j, k, ivar, indx;
  CCTK_REAL al, be, A, B, X, R, x, r, phi, y, z, Am1, *values;
  derivs U;
  CCTK_REAL *sources_point;

  values = dvector (0, nvar - 1);
  allocate_derivs (&U, nvar);

  sources_point = calloc(nvar         , sizeof(CCTK_REAL));

  Derivatives_AB3 (nvar, n1, n2, n3, v);
  CCTK_REAL psi, psi2, psi4, psi7, r_plus, r_minus;
  FILE *debugfile = NULL;
  if (do_residuum_debug_output  && CCTK_MyProc(cctkGH) == 0)
  {
    debugfile = fopen("res.dat", "a+");
    assert(debugfile);
    fprintf(debugfile, "# 1:x 2:y 3:A 4:B 5:psi 6:ham 7:ham_vac 8:ham_mat 9:mom 10:mom_vac 11:mom_mat 12:rho 13:S1 14:S2 15:S3 16:U.d0[0] 17:U.d0[1] 18:U.d0[2] 19:U.d0[3] 20:U.d1[1] 21:U.d1[2] 22:U.d1[3] 23:A11 24:A12 25:A13 26:A22 27:A23 28:A33 29:AijAij\n");
  }
  for (i = 0; i < n1; i++)
  {
    for (j = 0; j < n2; j++)
    {
      for (k = 0; k < n3; k++)
      {

        al = Pih * (2 * i + 1) / n1;
        A = -cos (al);
        be = Pih * (2 * j + 1) / n2;
        B = -cos (be);
        phi = 2. * Pi * k / n3;

        Am1 = A - 1;
        for (ivar = 0; ivar < nvar; ivar++)
        {
          indx = Index (ivar, i, j, k, nvar, n1, n2, n3);
          U.d0[ivar] = Am1 * v.d0[indx];        /* U*/
          U.d1[ivar] = v.d0[indx] + Am1 * v.d1[indx];        /* U_A*/
          U.d2[ivar] = Am1 * v.d2[indx];        /* U_B*/
          U.d3[ivar] = Am1 * v.d3[indx];        /* U_3*/
          U.d11[ivar] = 2 * v.d1[indx] + Am1 * v.d11[indx];        /* U_AA*/
          U.d12[ivar] = v.d2[indx] + Am1 * v.d12[indx];        /* U_AB*/
          U.d13[ivar] = v.d3[indx] + Am1 * v.d13[indx];        /* U_AB*/
          U.d22[ivar] = Am1 * v.d22[indx];        /* U_BB*/
          U.d23[ivar] = Am1 * v.d23[indx];        /* U_B3*/
          U.d33[ivar] = Am1 * v.d33[indx];        /* U_33*/
          sources_point[ivar] = sources[indx];
        }
        /* Calculation of (X,R) and*/
        /* (U_X, U_R, U_3, U_XX, U_XR, U_X3, U_RR, U_R3, U_33)*/
        AB_To_XR (nvar, A, B, &X, &R, U);
        /* Calculation of (x,r) and*/
        /* (U, U_x, U_r, U_3, U_xx, U_xr, U_x3, U_rr, U_r3, U_33)*/
        C_To_c (nvar, X, R, &x, &r, U);
        /* Calculation of (y,z) and*/
        /* (U, U_x, U_y, U_z, U_xx, U_xy, U_xz, U_yy, U_yz, U_zz)*/
        rx3_To_xyz (nvar, x, r, phi, &y, &z, U);
        NonLinEquations (nvar, sources_point,
                         A, B, X, R, x, r, phi, y, z, U, values);
        for (ivar = 0; ivar < nvar; ivar++)
        {
          indx = Index (ivar, i, j, k, nvar, n1, n2, n3);
          F[indx] = values[ivar] * FAC;
          /* if ((i<5) && ((j<5) || (j>n2-5)))*/
          /*     F[indx] = 0.0;*/
          u.d0[indx] = U.d0[ivar];        /*  U*/
          u.d1[indx] = U.d1[ivar];        /*      U_x*/
          u.d2[indx] = U.d2[ivar];        /*      U_y*/
          u.d3[indx] = U.d3[ivar];        /*      U_z*/
          u.d11[indx] = U.d11[ivar];        /*      U_xx*/
          u.d12[indx] = U.d12[ivar];        /*      U_xy*/
          u.d13[indx] = U.d13[ivar];        /*      U_xz*/
          u.d22[indx] = U.d22[ivar];        /*      U_yy*/
          u.d23[indx] = U.d23[ivar];        /*      U_yz*/
          u.d33[indx] = U.d33[ivar];        /*      U_zz*/
        }
        if (debugfile && (k==0))
        {
          r_plus = sqrt ((x - par_b) * (x - par_b) + y * y + z * z);
          r_minus = sqrt ((x + par_b) * (x + par_b) + y * y + z * z);
          psi = 1.+
                0.5 * par_m_plus  / r_plus +
                0.5 * par_m_minus / r_minus +
                U.d0[0];
          psi2 = psi * psi;
          psi4 = psi2 * psi2;
          psi7 = psi * psi2 * psi4;
          fprintf(debugfile,
                  "%.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g "
                  "%.16g %.16g %.16g %.16g %.16g %.16g %.16g "
                  "%.16g %.16g %.16g %.16g %.16g %.16g %.16g "
                  "%.16g %.16g %.16g %.16g %.16g %.16g %.16g "
                  "\n",
             (double)x, (double)y, (double)A, (double)B, (double)psi,
             (double)((U.d11[0] + U.d22[0] + U.d33[0] +
                      0.125*(/*BY_KKofxyz(x,y,z)+*/tilde_A_tilde_A(U)) / psi7 +
                      (2.0 * Pi / psi2/psi *
                       sources[Index(0,i,j,k,nvar,n1,n2,n3)])) * FAC),
             (double)((U.d11[0] + U.d22[0] + U.d33[0]+
                       0.125*(/*BY_KKofxyz(x,y,z)+*/tilde_A_tilde_A(U)) / psi7)
                       *FAC),
             (double)(-(2.0 * Pi / psi2/psi *
                       sources[Index(0,i,j,k,nvar,n1,n2,n3)]) * FAC),
             (nvar==1)?0.:
             (double)(U.d11[1] + U.d22[1] + U.d33[1]
                      + 1/3. * (U.d11[1] + U.d12[2] + U.d13[3])
                      - 8.0 * Pi *
                        sources[Index(1,i,j,k,nvar,n1,n2,n3)])*FAC,
             (nvar==1)?0.:
             (double)(U.d11[1] + U.d22[1] + U.d33[1]
                      + 1/3. * (U.d11[1] + U.d12[2] + U.d13[3]))*FAC,
             (nvar==1)?0.:
             (double)(- 8.0 * Pi *
                        sources[Index(1,i,j,k,nvar,n1,n2,n3)])*FAC,
             (double)sources[Index(0,i,j,k,nvar,n1,n2,n3)], /* 12 */
             (nvar==1)?0.:
             (double)sources[Index(1,i,j,k,nvar,n1,n2,n3)],
             (nvar==1)?0.:
             (double)sources[Index(2,i,j,k,nvar,n1,n2,n3)],
             (nvar==1)?0.:
             (double)sources[Index(3,i,j,k,nvar,n1,n2,n3)],
             (double)U.d0[0], /* 16 */
             (nvar==1)?0.:
             (double)U.d0[1],
             (nvar==1)?0.:
             (double)U.d0[2],
             (nvar==1)?0.:
             (double)U.d0[3],
             (nvar==1)?0.:    /* 20 */
             (double)U.d1[1],
             (nvar==1)?0.:
             (double)U.d1[2],
             (nvar==1)?0.:
             (double)U.d1[3],
             tilde_A(1,1,U),
             tilde_A(1,2,U),
             tilde_A(1,3,U),
             tilde_A(2,2,U),
             tilde_A(2,3,U),
             tilde_A(3,3,U),
             tilde_A_tilde_A(U)
             );
        }
      }
    }
    if (do_residuum_debug_output)
      fprintf(debugfile, "\n");
  }
  if (debugfile)
  {
    fprintf(debugfile, "\n");
    fclose(debugfile);
  }
  free(sources_point);
  free_dvector (values, 0, nvar - 1);
  free_derivs (&U, nvar);
}

/* --------------------------------------------------------------------------*/
void
J_times_dv (int nvar, int n1, int n2, int n3, derivs dv,
	    CCTK_REAL *Jdv, derivs u, CCTK_REAL *sources)
{				/*      Calculates the left hand sides of the non-linear equations F_m(v_n)=0*/
  /*      and the function u (u.d0[]) as well as its derivatives*/
  /*      (u.d1[], u.d2[], u.d3[], u.d11[], u.d12[], u.d13[], u.d22[], u.d23[], u.d33[])*/
  /*      at interior points and at the boundaries "+/-"*/
  DECLARE_CCTK_PARAMETERS;
  int i, j, k, ivar, indx;
  CCTK_REAL al, be, A, B, X, R, x, r, phi, y, z, Am1, *values;
  derivs dU, U;

  Derivatives_AB3 (nvar, n1, n2, n3, dv);

#pragma omp parallel private (values,dU,U,j,k,al,A,be,B,phi,X,R,x,r,y,z,Am1,ivar,indx)
  {
  values = dvector (0, nvar - 1);
  allocate_derivs (&dU, nvar);
  allocate_derivs (&U, nvar);
#pragma omp for schedule(dynamic)
  for (i = 0; i < n1; i++)
  {
    for (j = 0; j < n2; j++)
    {
      for (k = 0; k < n3; k++)
      {

	al = Pih * (2 * i + 1) / n1;
	A = -cos (al);
	be = Pih * (2 * j + 1) / n2;
	B = -cos (be);
	phi = 2. * Pi * k / n3;

	Am1 = A - 1;
	for (ivar = 0; ivar < nvar; ivar++)
	{
	  indx = Index (ivar, i, j, k, nvar, n1, n2, n3);
	  dU.d0[ivar] = Am1 * dv.d0[indx];	/* dU*/
	  dU.d1[ivar] = dv.d0[indx] + Am1 * dv.d1[indx];	/* dU_A*/
	  dU.d2[ivar] = Am1 * dv.d2[indx];	/* dU_B*/
	  dU.d3[ivar] = Am1 * dv.d3[indx];	/* dU_3*/
	  dU.d11[ivar] = 2 * dv.d1[indx] + Am1 * dv.d11[indx];	/* dU_AA*/
	  dU.d12[ivar] = dv.d2[indx] + Am1 * dv.d12[indx];	/* dU_AB*/
	  dU.d13[ivar] = dv.d3[indx] + Am1 * dv.d13[indx];	/* dU_AB*/
	  dU.d22[ivar] = Am1 * dv.d22[indx];	/* dU_BB*/
	  dU.d23[ivar] = Am1 * dv.d23[indx];	/* dU_B3*/
	  dU.d33[ivar] = Am1 * dv.d33[indx];	/* dU_33*/
	  U.d0[ivar] = u.d0[indx];	/* U   */
	  U.d1[ivar] = u.d1[indx];	/* U_x*/
	  U.d2[ivar] = u.d2[indx];	/* U_y*/
	  U.d3[ivar] = u.d3[indx];	/* U_z*/
	  U.d11[ivar] = u.d11[indx];	/* U_xx*/
	  U.d12[ivar] = u.d12[indx];	/* U_xy*/
	  U.d13[ivar] = u.d13[indx];	/* U_xz*/
	  U.d22[ivar] = u.d22[indx];	/* U_yy*/
	  U.d23[ivar] = u.d23[indx];	/* U_yz*/
	  U.d33[ivar] = u.d33[indx];	/* U_zz*/
	}
	/* Calculation of (X,R) and*/
	/* (dU_X, dU_R, dU_3, dU_XX, dU_XR, dU_X3, dU_RR, dU_R3, dU_33)*/
	AB_To_XR (nvar, A, B, &X, &R, dU);
	/* Calculation of (x,r) and*/
	/* (dU, dU_x, dU_r, dU_3, dU_xx, dU_xr, dU_x3, dU_rr, dU_r3, dU_33)*/
	C_To_c (nvar, X, R, &x, &r, dU);
	/* Calculation of (y,z) and*/
	/* (dU, dU_x, dU_y, dU_z, dU_xx, dU_xy, dU_xz, dU_yy, dU_yz, dU_zz)*/
	rx3_To_xyz (nvar, x, r, phi, &y, &z, dU);
	LinEquations (nvar, sources, A, B, X, R, x, r, phi, y, z, dU, U, values);
	for (ivar = 0; ivar < nvar; ivar++)
	{
	  indx = Index (ivar, i, j, k, nvar, n1, n2, n3);
	  Jdv[indx] = values[ivar] * FAC;
	}
      }
    }
  }
  free_dvector (values, 0, nvar - 1);
  free_derivs (&dU, nvar);
  free_derivs (&U, nvar);
  }
}

/* --------------------------------------------------------------------------*/
#define dv_d0(ind) (dv_ind == ind)
void
JFD_times_dv (int i, int j, int k, int nvar, int n1, int n2,
	      int n3, int dv_ind, derivs u, CCTK_REAL *values, CCTK_REAL *sources)
{				/* Calculates rows of the vector 'J(FD)*dv'.*/
  /* First row to be calculated: row = Index(0,      i, j, k; nvar, n1, n2, n3)*/
  /* Last  row to be calculated: row = Index(nvar-1, i, j, k; nvar, n1, n2, n3)*/
  /* These rows are stored in the vector JFDdv[0] ... JFDdv[nvar-1].*/
  DECLARE_CCTK_PARAMETERS;
  int ivar, indx;
  CCTK_REAL al, be, A, B, X, R, x, r, phi, y, z, Am1;
  CCTK_REAL sin_al, sin_al_i1, sin_al_i2, sin_al_i3, cos_al;
  CCTK_REAL sin_be, sin_be_i1, sin_be_i2, sin_be_i3, cos_be;
  CCTK_REAL dV0, dV1, dV2, dV3, dV11, dV12, dV13, dV22, dV23, dV33,
    ha, ga, ga2, hb, gb, gb2, hp, gp, gp2, gagb, gagp, gbgp;
  // dynamic arrays require C99
  CCTK_REAL dU_data[10][nvar], U_data[10][nvar];
  derivs dU, U;

  //allocate_derivs (&dU, nvar);
  //allocate_derivs (&U, nvar);
  dU.d0 = dU_data[0];
  dU.d1 = dU_data[1];
  dU.d2 = dU_data[2];
  dU.d3 = dU_data[3];
  dU.d11 = dU_data[4];
  dU.d12 = dU_data[5];
  dU.d13 = dU_data[6];
  dU.d22 = dU_data[7];
  dU.d23 = dU_data[8];
  dU.d33 = dU_data[9];

  U.d0 = U_data[0];
  U.d1 = U_data[1];
  U.d2 = U_data[2];
  U.d3 = U_data[3];
  U.d11 = U_data[4];
  U.d12 = U_data[5];
  U.d13 = U_data[6];
  U.d22 = U_data[7];
  U.d23 = U_data[8];
  U.d33 = U_data[9];

  if (k < 0)
    k = k + n3;
  if (k >= n3)
    k = k - n3;

  ha = Pi / n1;			/* ha: Stepsize with respect to (al)*/
  al = ha * (i + 0.5);
  A = -cos (al);
  ga = 1 / ha;
  ga2 = ga * ga;

  hb = Pi / n2;			/* hb: Stepsize with respect to (be)*/
  be = hb * (j + 0.5);
  B = -cos (be);
  gb = 1 / hb;
  gb2 = gb * gb;
  gagb = ga * gb;

  hp = 2 * Pi / n3;		/* hp: Stepsize with respect to (phi)*/
  phi = hp * j;
  gp = 1 / hp;
  gp2 = gp * gp;
  gagp = ga * gp;
  gbgp = gb * gp;


  sin_al = sin (al);
  sin_be = sin (be);
  sin_al_i1 = 1 / sin_al;
  sin_be_i1 = 1 / sin_be;
  sin_al_i2 = sin_al_i1 * sin_al_i1;
  sin_be_i2 = sin_be_i1 * sin_be_i1;
  sin_al_i3 = sin_al_i1 * sin_al_i2;
  sin_be_i3 = sin_be_i1 * sin_be_i2;
  cos_al = -A;
  cos_be = -B;

  Am1 = A - 1;
  for (ivar = 0; ivar < nvar; ivar++)
  {
    int iccc = Index (ivar, i, j, k, nvar, n1, n2, n3),
      ipcc = Index (ivar, i + 1, j, k, nvar, n1, n2, n3),
      imcc = Index (ivar, i - 1, j, k, nvar, n1, n2, n3),
      icpc = Index (ivar, i, j + 1, k, nvar, n1, n2, n3),
      icmc = Index (ivar, i, j - 1, k, nvar, n1, n2, n3),
      iccp = Index (ivar, i, j, k + 1, nvar, n1, n2, n3),
      iccm = Index (ivar, i, j, k - 1, nvar, n1, n2, n3),
      icpp = Index (ivar, i, j + 1, k + 1, nvar, n1, n2, n3),
      icmp = Index (ivar, i, j - 1, k + 1, nvar, n1, n2, n3),
      icpm = Index (ivar, i, j + 1, k - 1, nvar, n1, n2, n3),
      icmm = Index (ivar, i, j - 1, k - 1, nvar, n1, n2, n3),
      ipcp = Index (ivar, i + 1, j, k + 1, nvar, n1, n2, n3),
      imcp = Index (ivar, i - 1, j, k + 1, nvar, n1, n2, n3),
      ipcm = Index (ivar, i + 1, j, k - 1, nvar, n1, n2, n3),
      imcm = Index (ivar, i - 1, j, k - 1, nvar, n1, n2, n3),
      ippc = Index (ivar, i + 1, j + 1, k, nvar, n1, n2, n3),
      impc = Index (ivar, i - 1, j + 1, k, nvar, n1, n2, n3),
      ipmc = Index (ivar, i + 1, j - 1, k, nvar, n1, n2, n3),
      immc = Index (ivar, i - 1, j - 1, k, nvar, n1, n2, n3);
    /* Derivatives of (dv) w.r.t. (al,be,phi):*/
    dV0 = dv_d0(iccc);
    dV1 = 0.5 * ga * (dv_d0(ipcc) - dv_d0(imcc));
    dV2 = 0.5 * gb * (dv_d0(icpc) - dv_d0(icmc));
    dV3 = 0.5 * gp * (dv_d0(iccp) - dv_d0(iccm));
    dV11 = ga2 * (dv_d0(ipcc) + dv_d0(imcc) - 2 * dv_d0(iccc));
    dV22 = gb2 * (dv_d0(icpc) + dv_d0(icmc) - 2 * dv_d0(iccc));
    dV33 = gp2 * (dv_d0(iccp) + dv_d0(iccm) - 2 * dv_d0(iccc));
    dV12 =
      0.25 * gagb * (dv_d0(ippc) - dv_d0(ipmc) + dv_d0(immc) - dv_d0(impc));
    dV13 =
      0.25 * gagp * (dv_d0(ipcp) - dv_d0(imcp) + dv_d0(imcm) - dv_d0(ipcm));
    dV23 =
      0.25 * gbgp * (dv_d0(icpp) - dv_d0(icpm) + dv_d0(icmm) - dv_d0(icmp));
    /* Derivatives of (dv) w.r.t. (A,B,phi):*/
    dV11 = sin_al_i3 * (sin_al * dV11 - cos_al * dV1);
    dV12 = sin_al_i1 * sin_be_i1 * dV12;
    dV13 = sin_al_i1 * dV13;
    dV22 = sin_be_i3 * (sin_be * dV22 - cos_be * dV2);
    dV23 = sin_be_i1 * dV23;
    dV1 = sin_al_i1 * dV1;
    dV2 = sin_be_i1 * dV2;
    /* Derivatives of (dU) w.r.t. (A,B,phi):*/
    dU.d0[ivar] = Am1 * dV0;
    dU.d1[ivar] = dV0 + Am1 * dV1;
    dU.d2[ivar] = Am1 * dV2;
    dU.d3[ivar] = Am1 * dV3;
    dU.d11[ivar] = 2 * dV1 + Am1 * dV11;
    dU.d12[ivar] = dV2 + Am1 * dV12;
    dU.d13[ivar] = dV3 + Am1 * dV13;
    dU.d22[ivar] = Am1 * dV22;
    dU.d23[ivar] = Am1 * dV23;
    dU.d33[ivar] = Am1 * dV33;

    indx = Index (ivar, i, j, k, nvar, n1, n2, n3);
    U.d0[ivar] = u.d0[indx];	/* U   */
    U.d1[ivar] = u.d1[indx];	/* U_x*/
    U.d2[ivar] = u.d2[indx];	/* U_y*/
    U.d3[ivar] = u.d3[indx];	/* U_z*/
    U.d11[ivar] = u.d11[indx];	/* U_xx*/
    U.d12[ivar] = u.d12[indx];	/* U_xy*/
    U.d13[ivar] = u.d13[indx];	/* U_xz*/
    U.d22[ivar] = u.d22[indx];	/* U_yy*/
    U.d23[ivar] = u.d23[indx];	/* U_yz*/
    U.d33[ivar] = u.d33[indx];	/* U_zz*/
  }
  /* Calculation of (X,R) and*/
  /* (dU_X, dU_R, dU_3, dU_XX, dU_XR, dU_X3, dU_RR, dU_R3, dU_33)*/
  AB_To_XR (nvar, A, B, &X, &R, dU);
  /* Calculation of (x,r) and*/
  /* (dU, dU_x, dU_r, dU_3, dU_xx, dU_xr, dU_x3, dU_rr, dU_r3, dU_33)*/
  C_To_c (nvar, X, R, &x, &r, dU);
  /* Calculation of (y,z) and*/
  /* (dU, dU_x, dU_y, dU_z, dU_xx, dU_xy, dU_xz, dU_yy, dU_yz, dU_zz)*/
  rx3_To_xyz (nvar, x, r, phi, &y, &z, dU);
  LinEquations (nvar, sources, A, B, X, R, x, r, phi, y, z, dU, U, values);
  for (ivar = 0; ivar < nvar; ivar++)
    values[ivar] *= FAC;

  //free_derivs (&dU, nvar);
  //free_derivs (&U, nvar);
}
#undef dv_d0

/* --------------------------------------------------------------------------*/
void
SetMatrix_JFD (int nvar, int n1, int n2, int n3, derivs u,
	       int *ncols, int **cols, CCTK_REAL **Matrix, CCTK_REAL *sources)
{
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n1; i++)
  {
    for (int j = 0; j < n2; j++)
    {
      for (int k = 0; k < n3; k++)
      {
	for (int ivar = 0; ivar < nvar; ivar++)
	{
	  int row = Index (ivar, i, j, k, nvar, n1, n2, n3);
	  ncols[row] = 0;
	}
      }
    }
  }
#pragma omp parallel
  {
  CCTK_REAL *values, *sources_point;
  
  values = dvector (0, nvar - 1);
  sources_point = calloc(nvar, sizeof(CCTK_REAL));
#pragma omp for
  for (int i = 0; i < n1; i++)
  {
    for (int j = 0; j < n2; j++)
    {
      for (int k = 0; k < n3; k++)
      {
	for (int ivar = 0; ivar < nvar; ivar++)
	{
	  const int column = Index (ivar, i, j, k, nvar, n1, n2, n3);

          const CCTK_INT N1 = n1 - 1;
          const CCTK_INT N2 = n2 - 1;
          const CCTK_INT N3 = n3 - 1;

	  const int i_0 = maximum2 (0, i - 1);
	  const int i_1 = minimum2 (N1, i + 1);
	  const int j_0 = maximum2 (0, j - 1);
	  const int j_1 = minimum2 (N2, j + 1);
	  const int k_0 = k - 1;
	  const int k_1 = k + 1;
/*					i_0 = 0;
					i_1 = N1;
					j_0 = 0;
					j_1 = N2;
					k_0 = 0;
					k_1 = N3;*/

	  for (int i1 = i_0; i1 <= i_1; i1++)
	  {
	    for (int j1 = j_0; j1 <= j_1; j1++)
	    {
	      for (int k1 = k_0; k1 <= k_1; k1++)
	      {
		for (int ivar1 = 0; ivar1 < nvar; ivar1++)
                    sources_point[ivar1] = sources[Index(ivar1,i1,j1,k1,
                                                         nvar,n1,n2,n3)];
		JFD_times_dv (i1, j1, k1, nvar, n1, n2, n3,
			      column, u, values, sources_point);
		for (int ivar1 = 0; ivar1 < nvar; ivar1++)
		{
		  if (values[ivar1] != 0)
		  {
		    const int row = Index (ivar1, i1, j1, k1, nvar, n1, n2, n3);
                    int mcol;
#pragma omp critical
                    {
		    mcol = ncols[row];
		    ncols[row] += 1;
                    }
		    cols[row][mcol] = column;
		    Matrix[row][mcol] = values[ivar1];
		  }
		}
	      }
	    }
	  }

	}
      }
    }
  }
  free(sources_point);
  free_dvector (values, 0, nvar - 1);
  }
}

/* --------------------------------------------------------------------------*/
/* Calculates the value of v at an arbitrary position (A,B,phi)*/
CCTK_REAL
PunctEvalAtArbitPosition (int ivar, int nvar,
                          CCTK_REAL *v, CCTK_REAL A, CCTK_REAL B, CCTK_REAL phi,
			  int n1, int n2, int n3)
{
  int i, j, k, N;
  CCTK_REAL *p, *values1, **values2, result;

  N = maximum3 (n1, n2, n3);
  p = dvector (0, N);
  values1 = dvector (0, N);
  values2 = dmatrix (0, N, 0, N);

  for (k = 0; k < n3; k++)
  {
    for (j = 0; j < n2; j++)
    {
      for (i = 0; i < n1; i++)
	p[i] = v[ivar + nvar * (i + n1 * (j + n2 * k))];
      chebft_Zeros (p, n1, 0);
      values2[j][k] = chebev (-1, 1, p, n1, A);
    }
  }

  for (k = 0; k < n3; k++)
  {
    for (j = 0; j < n2; j++)
      p[j] = values2[j][k];
    chebft_Zeros (p, n2, 0);
    values1[k] = chebev (-1, 1, p, n2, B);
  }

  fourft (values1, n3, 0);
  result = fourev (values1, n3, phi);

  free_dvector (p, 0, N);
  free_dvector (values1, 0, N);
  free_dmatrix (values2, 0, N, 0, N);

  return result;
}

/* --------------------------------------------------------------------------*/
/* Calculates the value of v at an arbitrary position (A,B,phi), using
 * precomputed values */

CCTK_REAL *PEAAP_p[4] = {NULL, NULL, NULL, NULL};

void PunctEvalAtArbitPositionI(int nvar, CCTK_REAL *v, int n1, int n2, int n3, int buffer)
{
  int buffer_size = nvar*n1*n2*n3;
  if (!PEAAP_p[buffer])
    PEAAP_p[buffer] = (CCTK_REAL*)calloc(sizeof(CCTK_REAL), buffer_size);
  CCTK_REAL *p = dvector (0, n1);

  for (int ivar = 0; ivar < nvar; ivar++)
  for (int k = 0; k < n3; k++)
  {
    for (int j = 0; j < n2; j++)
    {
      for (int i = 0; i < n1; i++)
        p[i] = v[ivar + nvar * (i + n1 * (j + n2 * k))];
      chebft_Zeros (p, n1, 0);
      for (int i = 0; i < n1; i++)
        PEAAP_p[buffer][ivar + nvar * (i + n1 * (j + n2 * k))] = p[i];
    }
  }

  free_dvector (p, 0, n1);
}
void PunctEvalAtArbitPositionU()
{
  for (int buffer = 0; buffer < 4; buffer++)
  {
    free(PEAAP_p[buffer]);
    PEAAP_p[buffer] = NULL;
  }
}

CCTK_REAL
PunctEvalAtArbitPositionF (int ivar, int nvar,
                          CCTK_REAL *v, CCTK_REAL A, CCTK_REAL B, CCTK_REAL phi,
			  int n1, int n2, int n3, int buffer)
{
  int i, j, k, N;
  CCTK_REAL *p, *values1, **values2, result;

  N = maximum3 (n1, n2, n3);
  p = dvector (0, N);
  values1 = dvector (0, N);
  values2 = dmatrix (0, N, 0, N);

  for (k = 0; k < n3; k++)
  {
    for (j = 0; j < n2; j++)
    {
      for (i = 0; i < n1; i++)
        p[i] = PEAAP_p[buffer][ivar + nvar * (i + n1 * (j + n2 * k))];
      values2[j][k] = chebev (-1, 1, p, n1, A);
    }
  }

  for (k = 0; k < n3; k++)
  {
    for (j = 0; j < n2; j++)
      p[j] = values2[j][k];
    chebft_Zeros (p, n2, 0);
    values1[k] = chebev (-1, 1, p, n2, B);
  }

  fourft (values1, n3, 0);
  result = fourev (values1, n3, phi);

  free_dvector (p, 0, N);
  free_dvector (values1, 0, N);
  free_dmatrix (values2, 0, N, 0, N);

  return result;
}

/* --------------------------------------------------------------------------*/
/* Calculates the value of v at an arbitrary position (A,B,phi)*/
CCTK_REAL
PunctEvalCoeffsAtArbitPosition (CCTK_REAL *v, int ivar, CCTK_REAL A, CCTK_REAL B, CCTK_REAL phi,
        int nvar, int n1, int n2, int n3)
{
  int i, j, k;
  CCTK_REAL *q, *r, result;

  /* only do one memory allocation for both scratch vectors */
  q =  dvector (0, n2+n3); /* n2 elements for q, n3 for r */
  r =  q+n2;

  for (k = 0; k < n3; k++)
  {
    for (j = 0; j < n2; j++)
    {
      /* embed copy of chebev(a=-1,b=1,v,m=n1,x=A) from TP_utilities (for speed) */
      CCTK_REAL dip2, dip1, di; /* d_{i+2}, d_{i+1} and d_i */

      di = dip1 = 0;
      for(i = n1-1 ; i >= 1; i--)
      { 
        /* advance the coefficients */
        dip2 = dip1; 
        dip1 = di;
        di   = 2*A*dip1 - dip2 + v[ivar + nvar * (i + n1 * (j + n2 * k))];
      }

      q[j] = A*di - dip1 + 0.5*v[ivar + nvar * (0 + n1 * (j + n2 * k))];
    }
    r[k] = chebev (-1, 1, q, n2, B);
  }
  result = fourev (r, n3, phi);

  free_dvector (q, 0, n2+n3);

  return result;
}

/* --------------------------------------------------------------------------*/
/* calculates the derivatives with resprect to al, be, phi from derivates with
   respect to A, B and phi and stores them in vv */
void
calculate_derivs (int i, int j, int k, int ivar, int nvar, int n1, int n2,
		  int n3, derivs v, derivs vv)
{
  CCTK_REAL al = Pih * (2 * i + 1) / n1, be = Pih * (2 * j + 1) / n2,
    sin_al = sin (al), sin2_al = sin_al * sin_al, cos_al = cos (al),
    sin_be = sin (be), sin2_be = sin_be * sin_be, cos_be = cos (be);

  vv.d0[0] = v.d0[Index (ivar, i, j, k, nvar, n1, n2, n3)];
  vv.d1[0] = v.d1[Index (ivar, i, j, k, nvar, n1, n2, n3)] * sin_al;
  vv.d2[0] = v.d2[Index (ivar, i, j, k, nvar, n1, n2, n3)] * sin_be;
  vv.d3[0] = v.d3[Index (ivar, i, j, k, nvar, n1, n2, n3)];
  vv.d11[0] = v.d11[Index (ivar, i, j, k, nvar, n1, n2, n3)] * sin2_al
    + v.d1[Index (ivar, i, j, k, nvar, n1, n2, n3)] * cos_al;
  vv.d12[0] =
    v.d12[Index (ivar, i, j, k, nvar, n1, n2, n3)] * sin_al * sin_be;
  vv.d13[0] = v.d13[Index (ivar, i, j, k, nvar, n1, n2, n3)] * sin_al;
  vv.d22[0] = v.d22[Index (ivar, i, j, k, nvar, n1, n2, n3)] * sin2_be
    + v.d2[Index (ivar, i, j, k, nvar, n1, n2, n3)] * cos_be;
  vv.d23[0] = v.d23[Index (ivar, i, j, k, nvar, n1, n2, n3)] * sin_be;
  vv.d33[0] = v.d33[Index (ivar, i, j, k, nvar, n1, n2, n3)];
}

/* --------------------------------------------------------------------------*/
/* does a taylor expansion to second order */
CCTK_REAL
interpol (CCTK_REAL a, CCTK_REAL b, CCTK_REAL c, derivs v)
{
  return v.d0[0]
    + a * v.d1[0] + b * v.d2[0] + c * v.d3[0]
    + 0.5 * a * a * v.d11[0] + a * b * v.d12[0] + a * c * v.d13[0]
    + 0.5 * b * b * v.d22[0] + b * c * v.d23[0] + 0.5 * c * c * v.d33[0];
}

/* --------------------------------------------------------------------------*/
/* Calculates the value of v at an arbitrary position (x,y,z)*/
CCTK_REAL
PunctTaylorExpandAtArbitPosition (int ivar, int nvar, int n1,
                                  int n2, int n3, derivs v, CCTK_REAL x, CCTK_REAL y,
                                  CCTK_REAL z, const int Am1)
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL xs, ys, zs, rs2, phi, X, R, A, B, al, be, aux1, aux2, a, b, c,
    result, Ui;
  int i, j, k;
  derivs vv;
  allocate_derivs (&vv, 1);

  xs = x / par_b;
  ys = y / par_b;
  zs = z / par_b;
  rs2 = ys * ys + zs * zs;
  phi = atan2 (z, y);
  if (phi < 0)
    phi += 2 * Pi;

  aux1 = 0.5 * (xs * xs + rs2 - 1);
  aux2 = sqrt (aux1 * aux1 + rs2);
  X = asinh (sqrt (aux1 + aux2));
  R = asin (min(1.0, sqrt (-aux1 + aux2)));
  if (x < 0)
    R = Pi - R;

  /* exact coordinates */
  A = 2 * tanh (0.5 * X) - 1;
  B = tan (0.5 * R - Piq);
  al = Pi - acos (A);
  be = Pi - acos (B);

  /* index of nearest gridpoint */
  i = (int)(rint (al * n1 / Pi - 0.5));
  j = (int)(rint (be * n2 / Pi - 0.5));
  k = (int)(rint (0.5 * phi * n3 / Pi));

  /* Enforce array boundaries */
  i = (i>n1-1) ? n1-1 : (i<0) ? 0 : i;
  j = (j>n2-1) ? n2-1 : (j<0) ? 0 : j;
  k = (k>n3-1) ? n3-1 : (k<0) ? 0 : k;
  
  /* distance to nearest gridpoint from exact point */
  a = al - Pi * (i + 0.5) / n1;
  b = be - Pi * (j + 0.5) / n2;
  c = phi - 2 * Pi * k / n3;

  calculate_derivs (i, j, k, ivar, nvar, n1, n2, n3, v, vv);
  result = interpol (a, b, c, vv);
  free_derivs (&vv, 1);

  if (Am1)
    Ui = (A - 1) * result;
  else
    Ui = result;

  return Ui;
}

CCTK_REAL
PunctFDOAtArbitPosition (int ivar, int nvar, int n1,
                        int n2, int n3, CCTK_REAL *v, CCTK_REAL x, CCTK_REAL y,
                        CCTK_REAL z, const int Am1)
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL xs, ys, zs, rs2, phi, X, R, A, B, aux1, aux2, al, be;
  int i, j, k, ip, jp, kp;
  derivs vv;
  allocate_derivs (&vv, 1);

  xs = x / par_b;
  ys = y / par_b;
  zs = z / par_b;
  rs2 = ys * ys + zs * zs;
  phi = atan2 (z, y);
  if (phi < 0)
    phi += 2 * Pi;

  aux1 = 0.5 * (xs * xs + rs2 - 1);
  aux2 = sqrt (aux1 * aux1 + rs2);
  X = asinh (sqrt (aux1 + aux2));
  R = asin (min(1.0, sqrt (-aux1 + aux2)));
  if (x < 0)
    R = Pi - R;

  /* exact coordinates */
  A = 2 * tanh (0.5 * X) - 1;
  B = tan (0.5 * R - Piq);
  al = Pi - acos (A);
  be = Pi - acos (B);

  /* index of lower gridpoint */
  i = (int)(rint (al * n1 / Pi - 1.0));
  j = (int)(rint (be * n2 / Pi - 1.0));
  k = (int)(rint (0.5 * phi * n3 / Pi));

  /* Enforce array boundaries */
  i = (i>n1-1) ? n1-1 : (i<0) ? 0 : i;
  j = (j>n2-1) ? n2-1 : (j<0) ? 0 : j;
  k = (k>n3-1) ? n3-1 : (k<0) ? 0 : k;

  /* index of next point in positive direction */
  ip = i+1;
  jp = j+1;
  kp = (k+1)%n3;

  /* trilinear Interpolation */
  if ((ip < n1-1) && (jp < n2-1))
  {
    // Difference to lower point in al/be/phi grid
    CCTK_REAL id = al + cos(Pih * (2 * i + 1) / n1);
    CCTK_REAL jd = be + cos(Pih * (2 * j + 1) / n2);
    CCTK_REAL kd = phi - 2 * Pi * k / n3;
    if (kd < 0) kd += 2 * Pi;

    CCTK_REAL i1 = v[Index (ivar, i , j , k , nvar, n1, n2, n3)] * (1-kd) +
                   v[Index (ivar, i , j , kp, nvar, n1, n2, n3)] *    kd;
    CCTK_REAL i2 = v[Index (ivar, i , jp, k , nvar, n1, n2, n3)] * (1-kd) +
                   v[Index (ivar, i , jp, kp, nvar, n1, n2, n3)] *    kd;
    CCTK_REAL j1 = v[Index (ivar, ip, j , k , nvar, n1, n2, n3)] * (1-kd) +
                   v[Index (ivar, ip, j , kp, nvar, n1, n2, n3)] *    kd;
    CCTK_REAL j2 = v[Index (ivar, ip, jp, k , nvar, n1, n2, n3)] * (1-kd) +
                   v[Index (ivar, ip, jp, kp, nvar, n1, n2, n3)] *    kd;
    CCTK_REAL w1 = i1 * (1-jd) + i2 * jd;
    CCTK_REAL w2 = j1 * (1-jd) + j2 * jd;
    return         w1 * (1-id) + w2 * id;
  }
  return v[Index (ivar, i, j, k, nvar, n1, n2, n3)];
}

// Will write to members of U
static void
PunctFDAtArbitPosition2 (derivs U, int ivar, int nvar, int n1,
                        int n2, int n3, derivs v, CCTK_REAL x, CCTK_REAL y,
                        CCTK_REAL z, CCTK_REAL h[3], const int Am1)
{
    /* interpolate around point */
    CCTK_REAL cube[3][3][3];
    for (int i = -1 ; i <= 1 ; i++)
      for (int j = -1 ; j <= 1 ; j++)
        for (int k = -1 ; k <= 1 ; k++)
          if (i && !j && !k || !i && j && !k || !i && !j && k || !i && !j && !k)
            cube[i+1][j+1][k+1] = PunctTaylorExpandAtArbitPosition(ivar, nvar, n1, n2, n3,
                                      v, x+i*h[0], y+j*h[1], z+k*h[2], Am1);
    U.d0[ivar] =  cube[1][1][1];
    U.d1[ivar] = (cube[2][1][1] - cube[0][1][1]) / ( 2 * h[0] );
    U.d2[ivar] = (cube[1][2][1] - cube[1][0][1]) / ( 2 * h[1] );
    U.d3[ivar] = (cube[1][1][2] - cube[1][1][0]) / ( 2 * h[2] );
}

static void
PunctFDAtArbitPosition4 (derivs U, int ivar, int nvar, int n1,
                        int n2, int n3, derivs v, CCTK_REAL x, CCTK_REAL y,
                        CCTK_REAL z, CCTK_REAL h[3], const int Am1)
{
    /* interpolate around point */
    CCTK_REAL cube[5][5][5];
    for (int i = -2 ; i <= 2 ; i++)
      for (int j = -2 ; j <= 2 ; j++)
        for (int k = -2 ; k <= 2 ; k++)
          if (i && !j && !k || !i && j && !k || !i && !j && k || !i && !j && !k)
            cube[i+2][j+2][k+2] = PunctTaylorExpandAtArbitPosition(ivar, nvar, n1, n2, n3,
                                      v, x+i*h[0], y+j*h[1], z+k*h[2], Am1);
    U.d0[ivar] =  cube[2][2][2];
    U.d1[ivar] = (cube[0][2][2]/4-cube[1][2][2]*2+cube[3][2][2]*2-cube[4][2][2]/4)/(3*h[0]);
    U.d2[ivar] = (cube[2][0][2]/4-cube[2][1][2]*2+cube[2][3][2]*2-cube[2][4][2]/4)/(3*h[1]);
    U.d3[ivar] = (cube[2][2][0]/4-cube[2][2][1]*2+cube[2][2][3]*2-cube[2][2][4]/4)/(3*h[1]);
}

void
PunctFDAtArbitPosition (derivs U, int ivar, int nvar, int n1,
                        int n2, int n3, derivs v, CCTK_REAL x, CCTK_REAL y,
                        CCTK_REAL z, CCTK_REAL h[3], const int Am1)
{
    PunctFDAtArbitPosition2(U, ivar, nvar, n1, n2, n3, v, x, y, z, h, Am1);
}

void
PunctFDAtArbitPositionEval (derivs U, int ivar, int nvar, int n1,
                        int n2, int n3, derivs v, CCTK_REAL x, CCTK_REAL y,
                        CCTK_REAL z, CCTK_REAL h[3], const int Am1)
{
    /* interpolate around point */
    CCTK_REAL cube[3][3][3];
    for (int i = -1 ; i <= 1 ; i++)
      for (int j = -1 ; j <= 1 ; j++)
        for (int k = -1 ; k <= 1 ; k++)
          if (i && !j && !k || !i && j && !k || !i && !j && k || !i && !j && !k)
            cube[i+1][j+1][k+1] = PunctIntPolAtArbitPositionF(ivar, nvar, n1, n2, n3,
                                      v.d0, x+i*h[0], y+j*h[1], z+k*h[2], Am1, 0);
    U.d0[ivar] =  cube[1][1][1];
    U.d1[ivar] = (cube[2][1][1] - cube[0][1][1]) / ( 2 * h[0] );
    U.d2[ivar] = (cube[1][2][1] - cube[1][0][1]) / ( 2 * h[1] );
    U.d3[ivar] = (cube[1][1][2] - cube[1][1][0]) / ( 2 * h[2] );
}

/* --------------------------------------------------------------------------*/
/* Calculates the value of v at an arbitrary position (x,y,z)*/
CCTK_REAL
PunctIntPolAtArbitPosition (int ivar, int nvar, int n1,
			    int n2, int n3, double *v, CCTK_REAL x, CCTK_REAL y,
			    CCTK_REAL z, const int Am1)
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL xs, ys, zs, rs2, phi, X, R, A, B, aux1, aux2, result, Ui;

  xs = x / par_b;
  ys = y / par_b;
  zs = z / par_b;
  rs2 = ys * ys + zs * zs;
  phi = atan2 (z, y);
  if (phi < 0)
    phi += 2 * Pi;

  aux1 = 0.5 * (xs * xs + rs2 - 1);
  aux2 = sqrt (aux1 * aux1 + rs2);
  X = asinh (sqrt (aux1 + aux2));
  R = asin (min(1.0, sqrt (-aux1 + aux2)));
  if (x < 0)
    R = Pi - R;

  A = 2 * tanh (0.5 * X) - 1;
  B = tan (0.5 * R - Piq);

  result = PunctEvalAtArbitPosition (ivar, nvar, v, A, B, phi, n1, n2, n3);

  if (Am1)
    Ui = (A - 1) * result;
  else
    Ui = result;

  return Ui;
}

/* --------------------------------------------------------------------------*/
/* Calculates the value of v at an arbitrary position (x,y,z) using cache */
CCTK_REAL
PunctIntPolAtArbitPositionF (int ivar, int nvar, int n1,
			    int n2, int n3, double *v, CCTK_REAL x, CCTK_REAL y,
			    CCTK_REAL z, const int Am1, int buffer)
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL xs, ys, zs, rs2, phi, X, R, A, B, aux1, aux2, result, Ui;

  xs = x / par_b;
  ys = y / par_b;
  zs = z / par_b;
  rs2 = ys * ys + zs * zs;
  phi = atan2 (z, y);
  if (phi < 0)
    phi += 2 * Pi;

  aux1 = 0.5 * (xs * xs + rs2 - 1);
  aux2 = sqrt (aux1 * aux1 + rs2);
  X = asinh (sqrt (aux1 + aux2));
  R = asin (min(1.0, sqrt (-aux1 + aux2)));
  if (x < 0)
    R = Pi - R;

  A = 2 * tanh (0.5 * X) - 1;
  B = tan (0.5 * R - Piq);

  result = PunctEvalAtArbitPositionF (ivar, nvar, v, A, B, phi, n1, n2, n3, buffer);

  if (Am1)
    Ui = (A - 1) * result;
  else
    Ui = result;

  return Ui;
}
/* --------------------------------------------------------------------------*/
/* Calculates the value of v at an arbitrary position (x,y,z)*/
CCTK_REAL
PunctIntPolCoeffsAtArbitPosition (int ivar, int nvar, int n1,
			    int n2, int n3, double *v, CCTK_REAL x, CCTK_REAL y,
			    CCTK_REAL z, int Am1)
{
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL xs, ys, zs, rs2, phi, X, R, A, B, aux1, aux2, result, Ui;

  xs = x / par_b;
  ys = y / par_b;
  zs = z / par_b;
  rs2 = ys * ys + zs * zs;
  phi = atan2 (z, y);
  if (phi < 0)
    phi += 2 * Pi;

  aux1 = 0.5 * (xs * xs + rs2 - 1);
  aux2 = sqrt (aux1 * aux1 + rs2);
  X = asinh (sqrt (aux1 + aux2));
  R = asin (min(1.0, sqrt (-aux1 + aux2)));
  if (x < 0)
    R = Pi - R;

  A = 2 * tanh (0.5 * X) - 1;
  B = tan (0.5 * R - Piq);

  result = PunctEvalCoeffsAtArbitPosition (v, ivar, A, B, phi, nvar, n1, n2, n3);

  if (Am1)
    Ui = (A - 1) * result;
  else
    Ui = result;

  return Ui;
}

/* --------------------------------------------------------------------------*/
