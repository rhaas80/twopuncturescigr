/* TwoPunctures:  File  "TwoPunctures.c"*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "TP_utilities.h"
#include "TwoPunctures.h"



/* Swap two variables */
static inline
void swap (CCTK_REAL * restrict const a, CCTK_REAL * restrict const b)
{
  CCTK_REAL const t = *a; *a=*b; *b=t;
}
#undef SWAP
#define SWAP(a,b) (swap(&(a),&(b)))



static
void set_initial_guess(CCTK_POINTER_TO_CONST cctkGH,
                       int nvar, derivs v)
{
  DECLARE_CCTK_PARAMETERS;

  int n1 = npoints_A, n2 = npoints_B, n3 = npoints_phi;

  CCTK_REAL *s_x, *s_y, *s_z;
  CCTK_REAL al, A, Am1, be, B, phi, R, r, X;
  CCTK_INT i, j, k, i3D, indx;
  derivs U;
  FILE *debug_file;

  s_x    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
  s_y    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
  s_z    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
  allocate_derivs (&U, nvar);
  for (int ivar = 0; ivar < nvar; ivar++)
    for (i = 0; i < n1; i++)
      for (j = 0; j < n2; j++)
        for (k = 0; k < n3; k++)
        {
          i3D = Index(ivar,i,j,k,1,n1,n2,n3);

          al = Pih * (2 * i + 1) / n1;
          A = -cos (al);
          be = Pih * (2 * j + 1) / n2;
          B = -cos (be);
          phi = 2. * Pi * k / n3;

          /* Calculation of (X,R)*/
          AB_To_XR (nvar, A, B, &X, &R, U);
          /* Calculation of (x,r)*/
          C_To_c (nvar, X, R, &(s_x[i3D]), &r, U);
          /* Calculation of (y,z)*/
          rx3_To_xyz (nvar, s_x[i3D], r, phi, &(s_y[i3D]), &(s_z[i3D]), U);
        }
  Set_Initial_Guess_for_u(cctkGH, n1*n2*n3, v.d0, s_x, s_y, s_z);
  for (int ivar = 0; ivar < nvar; ivar++)
    for (i = 0; i < n1; i++)
      for (j = 0; j < n2; j++)
        for (k = 0; k < n3; k++)
        {
          indx = Index(ivar,i,j,k,1,n1,n2,n3);
          v.d0[indx]/=(-cos(Pih * (2 * i + 1) / n1)-1.0); /* A-1 */
        }
  Derivatives_AB3 (nvar, n1, n2, n3, v);
#if 0 /* code below is full of bugs (eg indx = Index(ivar,i,j,0,1,n1,n2,n3); s_x[indx] = ...; for nvar>1!) */
  if (do_initial_debug_output && CCTK_MyProc(cctkGH) == 0)
  {
    debug_file=fopen("initial.dat", "w");
    assert(debug_file);
    for (int ivar = 0; ivar < nvar; ivar++)
      for (i = 0; i < n1; i++)
        for (j = 0; j < n2; j++)
        {
          al = Pih * (2 * i + 1) / n1;
          A = -cos (al);
          Am1 = A -1.0;
          be = Pih * (2 * j + 1) / n2;
          B = -cos (be);
          phi = 0.0;
          indx = Index(ivar,i,j,0,1,n1,n2,n3);
          U.d0[0] = Am1 * v.d0[indx];        /* U*/
          U.d1[0] = v.d0[indx] + Am1 * v.d1[indx];        /* U_A*/
          U.d2[0] = Am1 * v.d2[indx];        /* U_B*/
          U.d3[0] = Am1 * v.d3[indx];        /* U_3*/
          U.d11[0] = 2 * v.d1[indx] + Am1 * v.d11[indx];        /* U_AA*/
          U.d12[0] = v.d2[indx] + Am1 * v.d12[indx];        /* U_AB*/
          U.d13[0] = v.d3[indx] + Am1 * v.d13[indx];        /* U_AB*/
          U.d22[0] = Am1 * v.d22[indx];        /* U_BB*/
          U.d23[0] = Am1 * v.d23[indx];        /* U_B3*/
          U.d33[0] = Am1 * v.d33[indx];        /* U_33*/
        /* Calculation of (X,R)*/
        AB_To_XR (nvar, A, B, &X, &R, U);
        /* Calculation of (x,r)*/
        C_To_c (nvar, X, R, &(s_x[indx]), &r, U);
        /* Calculation of (y,z)*/
        rx3_To_xyz (nvar, s_x[i3D], r, phi, &(s_y[indx]), &(s_z[indx]), U);
        fprintf(debug_file,
                "%.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g "
                "%.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g\n",
                (double)s_x[indx], (double)s_y[indx],
                (double)A,(double)B,
                (double)U.d0[0],
                (double)(-cos(Pih * (2 * i + 1) / n1)-1.0),
                (double)U.d1[0],
                (double)U.d2[0],
                (double)U.d3[0],
                (double)U.d11[0],
                (double)U.d22[0],
                (double)U.d33[0],
                (double)v.d0[indx],
                (double)v.d1[indx],
                (double)v.d2[indx],
                (double)v.d3[indx],
                (double)v.d11[indx],
                (double)v.d22[indx],
                (double)v.d33[indx]
                );
        }
    fprintf(debug_file, "\n\n");
    for (i=n2-10; i<n2; i++)
    {
      CCTK_REAL d;
      indx = Index(0,0,i,0,1,n1,n2,n3);
      d = PunctIntPolAtArbitPosition(0, nvar, n1, n2, n3, v.d0,
              s_x[indx], 0.0, 0.0, 1);
      fprintf(debug_file, "%.16g %.16g\n",
                (double)s_x[indx], (double)d);
    }
    fprintf(debug_file, "\n\n");
    for (i=n2-10; i<n2-1; i++)
    {
      CCTK_REAL d;
      int ip= Index(0,0,i+1,0,1,n1,n2,n3);
      indx = Index(0,0,i,0,1,n1,n2,n3);
      for (j=-10; j<10; j++)
      {
        d = PunctIntPolAtArbitPosition(0, nvar, n1, n2, n3, v.d0,
                s_x[indx]+(s_x[ip]-s_x[indx])*j/10,
                0.0, 0.0, 1);
        fprintf(debug_file, "%.16g %.16g\n",
                (double)(s_x[indx]+(s_x[ip]-s_x[indx])*j/10), (double)d);
      }
    }
    fprintf(debug_file, "\n\n");
    for (i = 0; i < n1; i++)
      for (j = 0; j < n2; j++)
      {
        X = 2*(2.0*i/n1-1.0);
        R = 2*(1.0*j/n2);
        if (X*X+R*R > 1.0)
        {
          C_To_c (nvar, X, R, &(s_x[indx]), &r, U);
          rx3_To_xyz (nvar, s_x[i3D], r, phi, &(s_y[indx]), &(s_z[indx]), U);
          *U.d0  = s_x[indx]*s_x[indx];
          *U.d1  = 2*s_x[indx];
          *U.d2  = 0.0;
          *U.d3 = 0.0;
          *U.d11 = 2.0;
          *U.d22 = 0.0;
          *U.d33 = *U.d12 = *U.d23 = *U.d13 = 0.0;
          C_To_c (nvar, X, R, &(s_x[indx]), &r, U);
          fprintf(debug_file,
                  "%.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g\n",
                  (double)s_x[indx], (double)r, (double)X, (double)R, (double)U.d0[0],
                  (double)U.d1[0],
                  (double)U.d2[0],
                  (double)U.d3[0],
                  (double)U.d11[0],
                  (double)U.d22[0],
                  (double)U.d33[0]);
        }
      }
    fclose(debug_file);
  }
#endif
  free(s_z);
  free(s_y);
  free(s_x);
  free_derivs (&U, nvar);
  /*exit(0);*/
}

/* read the sources from external thorn and store in returned array */
CCTK_REAL *read_sources(CCTK_POINTER_TO_CONST cctkGH,
                        int nvar, int n1, int n2, int n3)
{
    DECLARE_CCTK_PARAMETERS;
    CCTK_REAL *sources, *sources_tmp;
    CCTK_REAL *s_x, *s_y, *s_z;
    CCTK_REAL al, be, A, B, X, R, r, phi, Am1;
    int i,j,k,indx;
    CCTK_INT i3D;
    derivs U;

    sources       = calloc(nvar*n1*n2*n3, sizeof(CCTK_REAL));
    if (!use_sources)
    {
      for (i = 0; i < n1; i++)
        for (j = 0; j < n2; j++)
          for (k = 0; k < n3; k++)
            for (int ivar=0; ivar<nvar; ivar++)
              sources[Index(ivar,i,j,k,nvar,n1,n2,n3)]=0.0;
      return sources;
    }

    allocate_derivs (&U, nvar);

    s_x    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
    s_y    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
    s_z    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
    if (nvar>1)
      sources_tmp   = calloc(   1*n1*n2*n3, sizeof(CCTK_REAL));
    for (i = 0; i < n1; i++)
      for (j = 0; j < n2; j++)
        for (k = 0; k < n3; k++)
        {
          i3D = Index(0,i,j,k,1,n1,n2,n3);

          al = Pih * (2 * i + 1) / n1;
          A = -cos (al);
          be = Pih * (2 * j + 1) / n2;
          B = -cos (be);
          phi = 2. * Pi * k / n3;

          Am1 = A - 1;
          for (int ivar = 0; ivar < nvar; ivar++)
          {
            indx = Index (ivar, i, j, k, nvar, n1, n2, n3);
            U.d0[ivar] = 
            U.d1[ivar] = 
            U.d2[ivar] = 
            U.d3[ivar] = 
            U.d11[ivar] = 
            U.d12[ivar] = 
            U.d13[ivar] = 
            U.d22[ivar] = 
            U.d23[ivar] = 
            U.d33[ivar] = 0.0;
          }
          /* Calculation of (X,R) and*/
          /* (U_X, U_R, U_3, U_XX, U_XR, U_X3, U_RR, U_R3, U_33)*/
          AB_To_XR (nvar, A, B, &X, &R, U);
          /* Calculation of (x,r) and*/
          /* (U, U_x, U_r, U_3, U_xx, U_xr, U_x3, U_rr, U_r3, U_33)*/
          C_To_c (nvar, X, R, &(s_x[i3D]), &r, U);
          /* Calculation of (y,z) and*/
          /* (U, U_x, U_y, U_z, U_xx, U_xy, U_xz, U_yy, U_yz, U_zz)*/
          rx3_To_xyz (nvar, s_x[i3D], r, phi, &(s_y[i3D]), &(s_z[i3D]), U);
          /* offset */
          s_x[i3D] += center_offset[0];
          s_y[i3D] += center_offset[1];
          s_z[i3D] += center_offset[2];
        }
    /* Shortcut for nvar==1 */
    if (nvar==1)
      Set_Rho_ADM(cctkGH, n1*n2*n3, sources, s_x, s_y, s_z);
    else for (int ivar=0; ivar<nvar; ivar++)
    {
      switch (ivar)
      {
        case 0:
          Set_Rho_ADM(cctkGH, n1*n2*n3, sources_tmp, s_x, s_y, s_z);
          break;
        case 1:
          Set_Momentum_Source(cctkGH, (CCTK_INT)0,
                              n1*n2*n3, sources_tmp, s_x, s_y, s_z);
          break;
        case 2:
          Set_Momentum_Source(cctkGH, (CCTK_INT)1,
                              n1*n2*n3, sources_tmp, s_x, s_y, s_z);
          break;
        case 3:
          Set_Momentum_Source(cctkGH, (CCTK_INT)2,
                              n1*n2*n3, sources_tmp, s_x, s_y, s_z);
          break;
        default:
          CCTK_WARN(0, "Sorry, I do not know how to handle nvar>4, exit.");
      }
      for (i = 0; i < n1; i++)
        for (j = 0; j < n2; j++)
          for (k = 0; k < n3; k++)
          {
            /* Copy into final array */
            sources[Index (ivar, i, j, k, nvar, n1, n2, n3)]=
              sources_tmp[Index(0,i,j,k,1,n1,n2,n3)];
          }
    }
    free(s_z);
    free(s_y);
    free(s_x);
    free_derivs (&U, nvar);
    if (nvar>1)
      free(sources_tmp);
    return sources;
}

/* update the sources from external thorn and return true if we want further iterations */
CCTK_INT TP_Update_Sources(CCTK_ARGUMENTS, CCTK_REAL *sources, const derivs v,
                           int nvar, int n1, int n2, int n3)
{
    DECLARE_CCTK_PARAMETERS;
    DECLARE_CCTK_ARGUMENTS;

    CCTK_REAL *sources_tmp, *psi1;
    CCTK_REAL *s_x, *s_y, *s_z;
    CCTK_INT iterate;

    if(!rescale_metric)
      return 0;

    assert(use_sources);
    if(swap_xz) {
      CCTK_WARN(0, "swap_xz not tested. If you want to use, comment me out.");
    }

    psi1   =calloc(n1*n2*n3, sizeof(CCTK_REAL));
    assert(psi1);

    s_x    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
    s_y    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
    s_z    =calloc(n1*n2*n3, sizeof(CCTK_REAL));
    assert(s_x && s_y && s_z);

    sources_tmp   = calloc(4*n1*n2*n3, sizeof(CCTK_REAL));
    assert(sources_tmp);
#pragma omp parallel
    {
    derivs U;
    allocate_derivs (&U, nvar);
#pragma omp for
    for (int i = 0; i < n1; i++)
    {
      for (int j = 0; j < n2; j++)
      {
        for (int k = 0; k < n3; k++)
        {
          CCTK_REAL r_plus, r_minus;
          CCTK_REAL al, be, A, B, X, R, rr, phi, Am1;
          CCTK_INT i3D = Index(0,i,j,k,1,n1,n2,n3);

          al = Pih * (2 * i + 1) / n1;
          A = -cos (al);
          be = Pih * (2 * j + 1) / n2;
          B = -cos (be);
          phi = 2. * Pi * k / n3;

          Am1 = A - 1;
          for (int ivar = 0; ivar < nvar; ivar++)
          {
            int indx = Index (ivar, i, j, k, nvar, n1, n2, n3);
            U.d0[ivar] = Am1 * v.d0[indx];        /* U*/
            U.d1[ivar] = v.d0[indx] + Am1 * v.d1[indx];        /* U_A*/
            U.d2[ivar] = Am1 * v.d2[indx];        /* U_B*/
            U.d3[ivar] = Am1 * v.d3[indx];        /* U_3*/
            U.d11[ivar] = 2 * v.d1[indx] + Am1 * v.d11[indx];        /* U_AA*/
            U.d12[ivar] = v.d2[indx] + Am1 * v.d12[indx];        /* U_AB*/
            U.d13[ivar] = v.d3[indx] + Am1 * v.d13[indx];        /* U_AB*/
            U.d22[ivar] = Am1 * v.d22[indx];        /* U_BB*/
            U.d23[ivar] = Am1 * v.d23[indx];        /* U_B3*/
            U.d33[ivar] = Am1 * v.d33[indx];        /* U_33*/
            sources_tmp[Index(0,i,j,k,1,n1,n2,n3) + ivar*(n1*n2*n3)] =
              sources[Index (ivar, i, j, k, nvar, n1, n2, n3)];
          }
          /* Calculation of (X,R) and*/
          /* (U_X, U_R, U_3, U_XX, U_XR, U_X3, U_RR, U_R3, U_33)*/
          AB_To_XR (nvar, A, B, &X, &R, U);
          /* Calculation of (x,r) and*/
          /* (U, U_x, U_r, U_3, U_xx, U_xr, U_x3, U_rr, U_r3, U_33)*/
          C_To_c (nvar, X, R, &(s_x[i3D]), &rr, U);
          /* Calculation of (y,z) and*/
          /* (U, U_x, U_y, U_z, U_xx, U_xy, U_xz, U_yy, U_yz, U_zz)*/
          rx3_To_xyz (nvar, s_x[i3D], rr, phi, &(s_y[i3D]), &(s_z[i3D]), U);
          /* offset */
          s_x[i3D] += center_offset[0];
          s_y[i3D] += center_offset[1];
          s_z[i3D] += center_offset[2];
          r_plus = sqrt(pow(s_x[i3D] - par_b, 2) + pow(s_y[i3D], 2) + pow(s_z[i3D], 2));
          r_minus = sqrt(pow(s_x[i3D] + par_b, 2) + pow(s_y[i3D], 2) + pow(s_z[i3D], 2));
          psi1[i3D] = 1. + 0.5 * *mp / r_plus + 0.5 * *mm / r_minus + U.d0[0];
        }
      }
    }
    free_derivs (&U, nvar);
    }
    iterate = Update_Sources(cctkGH, n1*n2*n3, &sources_tmp[0*n1*n2*n3],
                             &sources_tmp[1*n1*n2*n3], &sources_tmp[2*n1*n2*n3],
                             &sources_tmp[3*n1*n2*n3], psi1, s_x,s_y,s_z);
    for (int ivar = 0; ivar < nvar; ivar++)
    {
      for (int i = 0; i < n1; i++)
      {
        for (int j = 0; j < n2; j++)
        {
          for (int k = 0; k < n3; k++)
          {
            /* Copy into final array */
            sources[Index(ivar, i, j, k, nvar, n1, n2, n3)] =
              sources_tmp[Index(0,i,j,k,1,n1,n2,n3) + ivar*(n1*n2*n3)];
          }
        }
      }
    }
    free(psi1);
    free(s_z);
    free(s_y);
    free(s_x);
    free(sources_tmp);
    return iterate;
}

//#define NEW_AB3
#ifdef NEW_get_AB3
void get_AB3(double par_b, double x, double y, double z,
                           double *A, double *B, double *phi)
{
    CCTK_REAL r_2, aux1, aux2, tmp1, tmp2;
    CCTK_REAL r = sqrt(x*x+y*y);
    r_2 = r*r;
    aux1 = 0.5 * (x*x+r_2)/(par_b*par_b) - 0.5;
    aux2 = sqrt(aux1*aux1 + r_2/(par_b*par_b));
    tmp1 = sqrt(aux1+aux2)+sqrt(aux1+aux2+1);
    *A = 4*tmp1 / (1+tmp1) - 3;
    tmp2 = sqrt(-aux1+aux2)/
           (1+sqrt(1-(-aux1+aux2)));
    *B = (tmp2-1) / (tmp2+1);
    // TODO: THIS IS WRONG
    *phi = 0.0;
}
#else
/* Calculates A,B and phi from x,y,z */
void get_AB3(double par_b, double x, double y, double z,
                           double *A, double *B, double *phi)
{
  CCTK_REAL xs, ys, zs, rs2, X, R, aux1, aux2;

  xs = x / par_b;
  ys = y / par_b;
  zs = z / par_b;
  rs2 = ys * ys + zs * zs;

  *phi = atan2 (z, y);
  if (*phi < 0)
    *phi += 2 * Pi;

  aux1 = 0.5 * (xs * xs + rs2 - 1);
  aux2 = sqrt (aux1 * aux1 + rs2);
  X = asinh (sqrt (aux1 + aux2));
  R = asin (min(1.0, sqrt (-aux1 + aux2)));
  if (x < 0)
    R = Pi - R;

  *A = 2 * tanh (0.5 * X) - 1;
  *B = tan (0.5 * R - Piq);
}
#endif

/* Calculates the derivatives with resprect to x,y and z from derivatives
   with respect to A, B and phi */
void Derivatives_xyz(int nvar, double par_b,
                     double x, double y, double z, derivs v,
                     double *A_, double *B_, int assume_Am1)
{
  double phi, Am1, r;

  CCTK_REAL xs, ys, zs, rs2, X, R, A, B, aux1, aux2;

  xs = x / par_b;
  ys = y / par_b;
  zs = z / par_b;
  rs2 = ys * ys + zs * zs;
  phi = atan2 (z, y);
  if (phi < 0)
    phi += 2 * Pi;


  aux1 = 0.5 * (xs * xs + rs2 - 1);
  aux2 = sqrt (aux1 * aux1 + rs2);
  X = asinh (sqrt (aux1 + aux2));
  R = asin (min(1.0, sqrt (-aux1 + aux2)));
  if (x < 0)
    R = Pi - R;

  A = 2 * tanh (0.5 * X) - 1;
  B = tan (0.5 * R - Piq);
  *A_ = A;
  *B_ = B;
  

  Am1 = A - 1;
  if (assume_Am1)
  for (int ivar = 0; ivar < nvar; ivar++)
  {
    v.d33[ivar] = Am1 * v.d33[ivar];        /* U_33*/
    v.d23[ivar] = Am1 * v.d23[ivar];        /* U_B3*/
    v.d22[ivar] = Am1 * v.d22[ivar];        /* U_BB*/
    v.d13[ivar] = v.d3[ivar] + Am1 * v.d13[ivar];        /* U_AB*/
    v.d12[ivar] = v.d2[ivar] + Am1 * v.d12[ivar];        /* U_AB*/
    v.d11[ivar] = 2 * v.d1[ivar] + Am1 * v.d11[ivar];        /* U_AA*/
    v.d3[ivar] = Am1 * v.d3[ivar];        /* U_3*/
    v.d2[ivar] = Am1 * v.d2[ivar];        /* U_B*/
    v.d1[ivar] = v.d0[ivar] + Am1 * v.d1[ivar];        /* U_A*/
    v.d0[ivar] = Am1 * v.d0[ivar];        /* U*/
  }
  /* Calculation of (X,R) and*/
  /* (U_X, U_R, U_3, U_XX, U_XR, U_X3, U_RR, U_R3, U_33)*/
  AB_To_XR (nvar, A, B, &X, &R, v);
  /* Calculation of (x,r) and*/
  /* (U, U_x, U_r, U_3, U_xx, U_xr, U_x3, U_rr, U_r3, U_33)*/
  save_C_To_c (nvar, X, R, &xs, &r, v);
  /* Calculation of (y,z) and*/
  /* (U, U_x, U_y, U_z, U_xx, U_xy, U_xz, U_yy, U_yz, U_zz)*/
  save_rx3_To_xyz (nvar, xs, r, phi, &ys, &zs, v);
}

/* -------------------------------------------------------------------*/
void
TwoPunctures (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  * mp = par_m_plus;
  * mm = par_m_minus;

  enum GRID_SETUP_METHOD { GSM_Taylor_expansion, GSM_Finite_Differences_Old, GSM_Finite_Differences, GSM_evaluation, GSM_Mixed, GSM_Mixed_evaluation };
  enum GRID_SETUP_METHOD gsm;

  int antisymmetric_lapse, averaged_lapse, pmn_lapse, brownsville_lapse;

  int nvar;
  int const n1 = npoints_A, n2 = npoints_B, n3 = npoints_phi;

  int imin[3], imax[3];
  int ntotal;
#if 0
  int percent10 = 0;
#endif
  /* TODO: add mechanism to free this memory in POSTINITIAL or so */
  static CCTK_REAL *F = NULL;
  static derivs u, v, v1,v2,v3, aij;
  static derivs v_coeffs;
 
  CCTK_REAL admMass;
  CCTK_REAL *sources;
  FILE *debugfile;

  chebft_Zeros_init_cache(n1);
  chebft_Zeros_init_cache(n2);
  fourft_init_cache(n3);

  /* only for debug 
  test_transform(); exit(0);
  */
  if (solve_momentum_constraint)
    nvar = 4;
  else
    nvar = 1;
  ntotal = n1 * n2 * n3 * nvar;

  if (! F) {
    /* Solve only when called for the first time */
    F = dvector (0, ntotal - 1);
    allocate_derivs (&u, ntotal);
    allocate_derivs (&v, ntotal);
    if (use_full_transform) {
      /* v_coeffs holds the Fourier/Chebyshev transform */
      v_coeffs.d0 = dvector(0, ntotal - 1);
      if (solve_momentum_constraint) {
        v_coeffs.d1 = dvector(0, ntotal - 1);
        v_coeffs.d2 = dvector(0, ntotal - 1);
        v_coeffs.d3 = dvector(0, ntotal - 1);
      }
    }

    if (use_sources) {
      CCTK_INFO ("Solving puncture equation for BH-NS/NS-NS system");
    } else {
      CCTK_INFO ("Solving puncture equation for BH-BH system");
    }
    CCTK_VInfo (CCTK_THORNSTRING, "b = %g", par_b);
    
    /* initialise to 0 */
    for (int j = 0; j < ntotal; j++)
    {
      v.d0[j] = 0.0;
      v.d1[j] = 0.0;
      v.d2[j] = 0.0;
      v.d3[j] = 0.0;
      v.d11[j] = 0.0;
      v.d12[j] = 0.0;
      v.d13[j] = 0.0;
      v.d22[j] = 0.0;
      v.d23[j] = 0.0;
      v.d33[j] = 0.0;
      if (use_full_transform) {
        v_coeffs.d0[j] = 0.0;
        if (solve_momentum_constraint) {
          v_coeffs.d1[j] = 0.0;
          v_coeffs.d2[j] = 0.0;
          v_coeffs.d3[j] = 0.0;
        }
      }
    }
    /* call for external initial guess */
    if (use_external_initial_guess)
    {
      set_initial_guess(cctkGH, nvar, v);
    }
    if (do_residuum_debug_output)
      remove("res.dat"); /* Standard C library call */
    sources = read_sources(cctkGH, nvar, n1, n2, n3);

    /* If bare masses are not given, iteratively solve for them given the 
       target ADM masses target_M_plus and target_M_minus and with initial 
       guesses given by par_m_plus and par_m_minus. */
    if(!(give_bare_mass)) {
      CCTK_REAL tmp, Mp_adm, Mm_adm, Mp_adm_err, Mm_adm_err, up, um;
      char valbuf[100];

      CCTK_REAL M_p = target_M_plus;
      CCTK_REAL M_m = target_M_minus;
      CCTK_VInfo (CCTK_THORNSTRING, "Attempting to find bare masses.");
      CCTK_VInfo (CCTK_THORNSTRING, "Target ADM masses: M_p=%g and M_m=%g",
                  (double) M_p, (double) M_m);
      CCTK_VInfo (CCTK_THORNSTRING, "ADM mass tolerance: %g", (double) adm_tol);
      /* Loop until both ADM masses are within adm_tol of their target */
      do {
        CCTK_VInfo (CCTK_THORNSTRING, "Bare masses: mp=%g, mm=%g",
                    (double)*mp, (double)*mm);
        Newton (cctkGH, nvar, n1, n2, n3, v, Newton_tol, 1, sources);
        F_of_v (cctkGH, nvar, n1, n2, n3, v, F, u, sources);

        up = PunctIntPolAtArbitPosition(0, nvar, n1, n2, n3, v.d0, par_b, 0., 0., 1);
        um = PunctIntPolAtArbitPosition(0, nvar, n1, n2, n3, v.d0,-par_b, 0., 0., 1);

        /* Calculate the ADM masses from the current bare mass guess */
        Mp_adm = (1 + up) * *mp + *mp * *mm / (4. * par_b);
        Mm_adm = (1 + um) * *mm + *mp * *mm / (4. * par_b);

        /* Check how far the current ADM masses are from the target */
        Mp_adm_err = fabs(M_p-Mp_adm);
        Mm_adm_err = fabs(M_m-Mm_adm);
        CCTK_VInfo (CCTK_THORNSTRING, "ADM mass error: M_p_err=%.4g, M_m_err=%.4g",
                    (double)Mp_adm_err, (double)Mm_adm_err);
        
        /* Invert the ADM mass equation and update the bare mass guess so that
           it gives the correct target ADM masses */
        tmp = -4*par_b*( 1 + um + up + um*up ) + 
                sqrt(16*par_b*M_m*(1 + um)*(1 + up) + 
                  pow(-M_m + M_p + 4*par_b*(1 + um)*(1 + up),2));
        *mp = (tmp + M_p - M_m)/(2.*(1 + up));
        *mm = (tmp - M_p + M_m)/(2.*(1 + um));
        
        /* Set the par_m_plus and par_m_minus parameters */
        sprintf (valbuf,"%.17g", (double) *mp);
        CCTK_ParameterSet ("par_m_plus", "twopunctures", valbuf);
        
        sprintf (valbuf,"%.17g", (double) *mm);
        CCTK_ParameterSet ("par_m_minus", "twopunctures", valbuf);
        
      } while ( (Mp_adm_err > adm_tol) ||
                (Mm_adm_err > adm_tol) );
                
      CCTK_VInfo (CCTK_THORNSTRING, "Found bare masses.");
    }
  int cp_found = 0;
  if(checkpoint_coefficients)
  {
  char fn[256];
  FILE *cp;
  sprintf(fn, "%s/cp_%d_%d_%d.dat", recover_dir, n1,n2,n3);
  cp = fopen(fn, "r");
  if (cp)
  {
    cp_found = 1;
    CCTK_VInfo (CCTK_THORNSTRING, "Reading coefficients from '%s'", fn);
    for (int i = 0; i < n1; i++)
     for (int j = 0; j < n2; j++)
      for (int k = 0; k < n3; k++)
	     for (int ivar = 0; ivar < nvar; ivar++)
       {
         const int ind = Index (ivar, i, j, k, nvar, n1, n2, n3);
         double cp_val;
         fscanf(cp, "%lf\n", &cp_val);
         v.d0[ind] = (CCTK_REAL)cp_val;
       }
    fclose(cp);
  }
  }


    if (!cp_found)
    {
      do {
        Newton (cctkGH, nvar, n1, n2, n3, v, Newton_tol, Newton_maxit, sources);
      } while(rescale_metric && TP_Update_Sources(CCTK_PASS_CTOC, sources, v, nvar, n1, n2, n3));
  if(checkpoint_coefficients && CCTK_MyProc(cctkGH) == 0)
  {
  char fn[256];
  FILE *cp;
  sprintf(fn, "%s/cp_%d_%d_%d.dat", checkpoint_dir, n1,n2,n3);
  cp = fopen(fn, "w");
    for (int i = 0; i < n1; i++)
    {
     for (int j = 0; j < n2; j++)
     {
      for (int k = 0; k < n3; k++)
      {
       for (int ivar = 0; ivar < nvar; ivar++)
       {
         const int ind = Index (ivar, i, j, k, nvar, n1, n2, n3);
         fprintf(cp, "%.18e ", (double)v.d0[ind]); /* doubles should have about 15 sig. figures */
       }
       fprintf(cp, "\n");
      }
      fprintf(cp, "\n");
     }
     fprintf(cp, "\n");
    }
  fclose(cp);
  CCTK_VInfo (CCTK_THORNSTRING, "Wrote coefficients to '%s'", fn);
  }
  }
  if(exit_after_spectral_solve)
    exit(0);

    /* need to recompute derivatives after recovery from checkpoints */
    F_of_v (cctkGH, nvar, n1, n2, n3, v, F, u, sources);
    if (use_full_transform)
    {
      TP_FullTransform(v.d0, v_coeffs.d0, nvar, n1, n2, n3);
      if (solve_momentum_constraint)
      {
        TP_FullTransform(v.d1, v_coeffs.d1, nvar, n1, n2, n3);
        TP_FullTransform(v.d2, v_coeffs.d2, nvar, n1, n2, n3);
        TP_FullTransform(v.d3, v_coeffs.d3, nvar, n1, n2, n3);
      }
    }

    CCTK_VInfo (CCTK_THORNSTRING,
                "The two puncture masses are mp=%.17g and mm=%.17g",
                (double) *mp, (double) *mm);


    /* print out ADM mass, eq.: \Delta M_ADM=2*r*u=4*b*V for A=1,B=0,phi=0 */
    admMass = (*mp + *mm
               - 4*par_b*PunctEvalAtArbitPosition(0, nvar, v.d0, 1, 0, 0, n1, n2, n3));;
    CCTK_VInfo (CCTK_THORNSTRING, "The total ADM mass is %g", (double) admMass);

  }

  if (CCTK_EQUALS(grid_setup_method, "Taylor expansion"))
  {
    gsm = GSM_Taylor_expansion;
  }
  else if (CCTK_EQUALS(grid_setup_method, "evaluation"))
  {
    gsm = GSM_evaluation;
  }
  else if (CCTK_EQUALS(grid_setup_method, "Finite Differences Old"))
  {
    gsm = GSM_Finite_Differences_Old;
  }
  else if (CCTK_EQUALS(grid_setup_method, "Finite Differences"))
  {
    gsm = GSM_Finite_Differences;
  }
  else if (CCTK_EQUALS(grid_setup_method, "Mixed Taylor + FD"))
  {
    gsm = GSM_Mixed;
  }
  else if (CCTK_EQUALS(grid_setup_method, "Mixed Evaluation + FD"))
  {
    gsm = GSM_Mixed_evaluation;
  }
  else
  {
    CCTK_WARN (0, "internal error");
    return; /* NOTREACHED */
  }

  antisymmetric_lapse = CCTK_EQUALS(initial_lapse, "twopunctures-antisymmetric");
  averaged_lapse = CCTK_EQUALS(initial_lapse, "twopunctures-averaged");
	pmn_lapse = CCTK_EQUALS(initial_lapse, "psi^n");
  if (pmn_lapse)
		CCTK_VInfo(CCTK_THORNSTRING, "Setting initial lapse to psi^%f profile.",
               (double)initial_lapse_psi_exponent);
  brownsville_lapse = CCTK_EQUALS(initial_lapse, "brownsville");
  if (brownsville_lapse)
    CCTK_VInfo(CCTK_THORNSTRING, 
               "Setting initial lapse to a Brownsville-style profile "
               "with exp %f.",
               (double)initial_lapse_psi_exponent);

  CCTK_INFO ("Interpolating result");
  if (CCTK_EQUALS(metric_type, "static conformal")) {
    if (CCTK_EQUALS(conformal_storage, "factor")) {
      *conformal_state = 1;
    } else if (CCTK_EQUALS(conformal_storage, "factor+derivs")) {
      *conformal_state = 2;
    } else if (CCTK_EQUALS(conformal_storage, "factor+derivs+2nd derivs")) {
      *conformal_state = 3;
    }
  } else {
    *conformal_state = 0;
  }

  for (int d = 0; d < 3; ++ d)
  {
    /*
    imin[d] = 0           + (cctk_bbox[2*d  ] ? 0 : cctk_nghostzones[d]);
    imax[d] = cctk_lsh[d] - (cctk_bbox[2*d+1] ? 0 : cctk_nghostzones[d]);
    */
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
#define FAC1 (1)
#define FAC2 (1)
#define FAC3 (1)
  if ((gsm == GSM_Taylor_expansion || gsm == GSM_Mixed) && solve_momentum_constraint)
  {
//    Derivatives_AB3 (nvar, n1, n2, n3, v);   not needed
    allocate_derivs (&v1, ntotal);
    allocate_derivs (&v2, ntotal);
    allocate_derivs (&v3, ntotal);
    allocate_derivs (&aij, n1*n2*n3*6);
    /* Copy 1st derivatives into new variables as values */
    for (int i = 0; i < n1; i++)
     for (int j = 0; j < n2; j++)
      for (int k = 0; k < n3; k++)
      {
       derivs U;
       allocate_derivs(&U, 4);
	     for (int ivar = 0; ivar < nvar; ivar++)
       {
         const int ind = Index (ivar, i, j, k, nvar, n1, n2, n3);
         double Am1 = -cos (Pih * (2 * i + 1) / n1) - 1.;

         double A = -cos(Pih * (2 * i + 1) / n1);
         double B = -cos(Pih * (2 * j + 1) / n2);
         double rr = par_b * (A+1.) / (1.-0.25*(A+1.)*(A+1.)) *
                             (1-B*B)/(1+B*B);
         double xx, phi = 2. * Pi * k / n3;
         double yy = rr * cos(phi);
         double zz = rr * sin(phi);
//#define NEW_TRY
         // get xyz derivatives of v without touching v itself
         derivs t;
         allocate_derivs(&t, 1);
         // get xx
         double X, R;
         AB_To_XR (1, A, B, &X, &R, t);
         C_To_c (1, X, R, &xx, &rr, t);
         // populate t
         t.d3[0] = v.d3[ind];
         t.d2[0] = v.d2[ind];
         t.d1[0] = v.d1[ind];
         t.d0[0] = v.d0[ind];
         Derivatives_xyz(1, par_b, xx, yy, zz, t, &A, &B, 1);
         // save xyz derivatives in v1, v2, v3
         v1.d0[ind] = t.d1[0] / FAC1;
         v2.d0[ind] = t.d2[0] / FAC2;
         v3.d0[ind] = t.d3[0] / FAC3;

         U.d1[ivar] = v1.d0[ind];
         U.d2[ivar] = v2.d0[ind];
         U.d3[ivar] = v3.d0[ind];

         free_derivs(&t, 1);
       }
       aij.d0[Index (0, i, j, k, 6, n1, n2, n3)] = tilde_A(1,1,U);
       aij.d0[Index (1, i, j, k, 6, n1, n2, n3)] = tilde_A(1,2,U);
       aij.d0[Index (2, i, j, k, 6, n1, n2, n3)] = tilde_A(1,3,U);
       aij.d0[Index (3, i, j, k, 6, n1, n2, n3)] = tilde_A(2,2,U);
       aij.d0[Index (4, i, j, k, 6, n1, n2, n3)] = tilde_A(2,3,U);
       aij.d0[Index (5, i, j, k, 6, n1, n2, n3)] = tilde_A(3,3,U);
       free_derivs(&U, 4);
        
#ifndef NEW_TRY
	     for (int ivar = 0; ivar < nvar; ivar++)
       {
         const int ind = Index (ivar, i, j, k, nvar, n1, n2, n3);
         v1.d0[ind] = v.d1[ind];
         v2.d0[ind] = v.d2[ind];
         v3.d0[ind] = v.d3[ind];
       }
#endif
     }
    /* Calculate their derivatives (now 2nd and 3rd of original values) */
    Derivatives_AB3 (nvar, n1, n2, n3, v1);
    Derivatives_AB3 (nvar, n1, n2, n3, v2);
    Derivatives_AB3 (nvar, n1, n2, n3, v3);
    Derivatives_AB3 (6, n1, n2, n3, aij);
  }

  if (do_misc_debug_output)
  {
    debugfile=fopen("debug.dat", "w");
  }

  PunctEvalAtArbitPositionI(nvar, v.d0, n1, n2, n3, 0);
  PunctEvalAtArbitPositionI(nvar, v.d1, n1, n2, n3, 1);
  PunctEvalAtArbitPositionI(nvar, v.d2, n1, n2, n3, 2);
  PunctEvalAtArbitPositionI(nvar, v.d3, n1, n2, n3, 3);
#pragma omp parallel firstprivate (gsm)
  {
  derivs U;
  allocate_derivs (&U, nvar);
#pragma omp for
  for (int ind = CCTK_GFINDEX3D (cctkGH, imin[0], imin[1], imin[2]) ; 
           ind <= CCTK_GFINDEX3D (cctkGH, imax[0]-1, imax[1]-1, imax[2]-1) ; 
           ind++)
//  for (int k = imin[2]; k < imax[2]; ++k)
  {
//    for (int j = imin[1]; j < imax[1]; ++j)
    {
//      for (int i = imin[0]; i < imax[0]; ++i)
      {
        CCTK_REAL Aij[3][3];
#if 0
        /* We can't output this when running in parallel */
        if (percent10 != 10*(i+j*cctk_lsh[0]+k*cctk_lsh[0]*cctk_lsh[1]) /
                            (cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2]))
        {
            percent10 = 10*(i+j*cctk_lsh[0]+k*cctk_lsh[0]*cctk_lsh[1]) /
                           (cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2]);
            CCTK_VInfo(CCTK_THORNSTRING, "%3d%% done", percent10*10);
        }
#endif

        //const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

        CCTK_REAL x1, y1, z1, A, B, phi;
        x1 = x[ind] - center_offset[0];
        y1 = y[ind] - center_offset[1];
        z1 = z[ind] - center_offset[2];

        /* We implement swapping the x and z coordinates as follows.
           The bulk of the code that performs the actual calculations
           is unchanged.  This code looks only at local variables.
           Before the bulk --i.e., here-- we swap all x and z tensor
           components, and after the code --i.e., at the end of this
           main loop-- we swap everything back.  */
        if (swap_xz) {
          /* Swap the x and z coordinates */
          SWAP (x1, z1);
        }
        get_AB3(par_b, x1, y1, z1, &A, &B, &phi);

        CCTK_REAL r_plus
          = sqrt(pow(x1 - par_b, 2) + pow(y1, 2) + pow(z1, 2));
        CCTK_REAL r_minus
          = sqrt(pow(x1 + par_b, 2) + pow(y1, 2) + pow(z1, 2));

        enum GRID_SETUP_METHOD tmp_gsm;
        tmp_gsm = gsm;
        if (gsm == GSM_Mixed || gsm == GSM_Mixed_evaluation)
        {
          CCTK_REAL al = Pi - acos (A);
          CCTK_REAL be = Pi - acos (B);
          /* index of closest gridpoint */
          int ii = (int)(rint (al * n1 / Pi + 0.5));
          int jj = (int)(rint (be * n2 / Pi + 0.5));

          //if ( (jj >= n2-15) || (ii >= n1-15) )
          if ( (jj >= n2-1) || (jj <= 1))
            gsm = GSM_Finite_Differences;
          else
            gsm = gsm == GSM_Mixed ? GSM_Taylor_expansion : GSM_evaluation;
        }
        
        switch (gsm)
        {
        case GSM_Taylor_expansion:
          /* For \psi we only need .d0 */
          if (!solve_momentum_constraint)
            U.d0[0] = PunctTaylorExpandAtArbitPosition
              (0, nvar, n1, n2, n3, v, x1, y1, z1, 0);
          else
          {
            /* For V^i we actually need .d? */
            for (int ivar=0; ivar < nvar; ivar++)
            {
              U.d0[ivar] = PunctTaylorExpandAtArbitPosition
                (ivar, nvar, n1, n2, n3, v, x1, y1, z1, 0);
              U.d1[ivar] = PunctTaylorExpandAtArbitPosition
                (ivar, nvar, n1, n2, n3, v1, x1, y1, z1, 0);
              U.d2[ivar] = PunctTaylorExpandAtArbitPosition
                (ivar, nvar, n1, n2, n3, v2, x1, y1, z1, 0);
              U.d3[ivar] = PunctTaylorExpandAtArbitPosition
                (ivar, nvar, n1, n2, n3, v3, x1, y1, z1, 0);
            /*
              double rr = sqrt(y1*y1+z1*z1);
              U.d1[ivar] *= FAC1;
              U.d2[ivar] *= FAC2;
              U.d3[ivar] *= FAC3;
              CCTK_REAL A, B, phi;
              get_AB3(par_b, x1, y1, z1, &A, &B, &phi);
              U.d1[ivar] = (U.d1[ivar]-U.d0[ivar])/(A-1.);
              U.d2[ivar] =  U.d2[ivar]/(A-1.);
              U.d3[ivar] =  U.d3[ivar]/(A-1.);*/
            }
            /*
            Aij[0][0] =PunctTaylorExpandAtArbitPosition
                (0, 6, n1, n2, n3, aij, x1, y1, z1, 0);
            Aij[0][1] =PunctTaylorExpandAtArbitPosition
                (1, 6, n1, n2, n3, aij, x1, y1, z1, 0);
            Aij[0][2] =PunctTaylorExpandAtArbitPosition
                (2, 6, n1, n2, n3, aij, x1, y1, z1, 0);
            Aij[1][1] =PunctTaylorExpandAtArbitPosition
                (3, 6, n1, n2, n3, aij, x1, y1, z1, 0);
            Aij[1][2] =PunctTaylorExpandAtArbitPosition
                (4, 6, n1, n2, n3, aij, x1, y1, z1, 0);
            Aij[2][2] =PunctTaylorExpandAtArbitPosition
                (5, 6, n1, n2, n3, aij, x1, y1, z1, 0);
            */
          }
          break;
        case GSM_Finite_Differences_Old:
          for (int ivar=0; ivar < nvar; ivar++)
          {
            U.d0[ivar] = PunctFDOAtArbitPosition
                         (ivar, nvar, n1, n2, n3, v.d0, x1, y1, z1, 0);
            U.d1[ivar] = PunctFDOAtArbitPosition
                         (ivar, nvar, n1, n2, n3, v.d1, x1, y1, z1, 0);
            U.d2[ivar] = PunctFDOAtArbitPosition
                         (ivar, nvar, n1, n2, n3, v.d2, x1, y1, z1, 0);
            U.d3[ivar] = PunctFDOAtArbitPosition
                         (ivar, nvar, n1, n2, n3, v.d3, x1, y1, z1, 0);
          }
          break;
        case GSM_Finite_Differences:
          {
            const double fac = 1./1;
            //CCTK_REAL h[3] = {cctk_delta_space[0]*fac, cctk_delta_space[1]*fac, cctk_delta_space[2]*fac};
            CCTK_REAL h[3] = {CCTK_DELTA_SPACE(0)*fac, CCTK_DELTA_SPACE(1)*fac, CCTK_DELTA_SPACE(2)*fac};
            if(tmp_gsm == GSM_Mixed || tmp_gsm == GSM_Finite_Differences)
            {
              for (int ivar=0; ivar < nvar; ivar++)
                PunctFDAtArbitPosition(U, ivar, nvar, n1, n2, n3, v, x1, y1, z1, h, 1);
            }
            else if(tmp_gsm == GSM_Mixed_evaluation)
            {
              for (int ivar=0; ivar < nvar; ivar++)
                PunctFDAtArbitPositionEval(U, ivar, nvar, n1, n2, n3, v, x1, y1, z1, h, 1);
            }
            else
              assert(0 == "Internal error");
          }
          break;
        case GSM_evaluation:
          /* For \psi we only need .d0 */
          if (!solve_momentum_constraint)
          {
            if (use_full_transform)
            {
              U.d0[0] = PunctIntPolCoeffsAtArbitPosition
                (0, nvar, n1, n2, n3, v_coeffs.d0, x1, y1, z1, 0);
            }
            else
            {
              U.d0[0] = PunctIntPolAtArbitPositionF
                (0, nvar, n1, n2, n3, v.d0, x1, y1, z1, 0, 0);
            }
          }
          else
          {
            /* For V^i we actually need .d? */
            for (int ivar=0; ivar < nvar; ivar++)
            {
              if (use_full_transform)
              {
                U.d0[ivar] = PunctIntPolCoeffsAtArbitPosition
                  (ivar, nvar, n1, n2, n3, v_coeffs.d0, x1, y1, z1, 0);
                U.d1[ivar] = PunctIntPolCoeffsAtArbitPosition
                  (ivar, nvar, n1, n2, n3, v_coeffs.d1, x1, y1, z1, 0);
                U.d2[ivar] = PunctIntPolCoeffsAtArbitPosition
                  (ivar, nvar, n1, n2, n3, v_coeffs.d2, x1, y1, z1, 0);
                U.d3[ivar] = PunctIntPolCoeffsAtArbitPosition
                  (ivar, nvar, n1, n2, n3, v_coeffs.d3, x1, y1, z1, 0);
              }
              else
              {
                U.d0[ivar] = PunctIntPolAtArbitPositionF
                  (ivar, nvar, n1, n2, n3, v.d0, x1, y1, z1, 0, 0);
                U.d1[ivar] = PunctIntPolAtArbitPositionF
                  (ivar, nvar, n1, n2, n3, v.d1, x1, y1, z1, 0, 1);
                U.d2[ivar] = PunctIntPolAtArbitPositionF
                  (ivar, nvar, n1, n2, n3, v.d2, x1, y1, z1, 0, 2);
                U.d3[ivar] = PunctIntPolAtArbitPositionF
                  (ivar, nvar, n1, n2, n3, v.d3, x1, y1, z1, 0, 3);
              }
              double rr = sqrt(y1*y1+z1*z1);
              U.d1[ivar] *= FAC1;
              U.d2[ivar] *= FAC2;
              U.d3[ivar] *= FAC3;
            }
          }
          break;
        default:
          assert (0);
        }
#if 0
        if (do_misc_debug_output)
          if (fabs(z1)<0.1)
              fprintf(debugfile, "%g %g %g %g %g %g %g ", x1,y1,z1,
                                 U.d0[1], U.d1[1], U.d2[1], U.d3[1]);
#endif
        /* We need the derivatives with resp. to x,y,z */
#ifndef NEW_TRY
        if (gsm != GSM_Finite_Differences)
          Derivatives_xyz(nvar, par_b, x1, y1, z1, U, &A, &B, 1);
#endif
        gsm = tmp_gsm;
#if 0
        if (do_misc_debug_output)
          if (fabs(z1)<0.1)
              fprintf(debugfile, "%g %g %g %g %g %g %g %g %g %g "
                                 "%g %g %g %g %g %g %g %g\n", A, B,
                                 U.d0[0], U.d1[0], U.d2[0], U.d3[0], /*10*/
                                 U.d0[1], U.d1[1], U.d2[1], U.d3[1], /*14*/
                                 U.d0[2], U.d1[2], U.d2[2], U.d3[2], /*18*/
                                 U.d0[3], U.d1[3], U.d2[3], U.d3[3]); /*22*/
#endif
        r_plus = pow (pow (r_plus, 4) + pow (TP_epsilon, 4), 0.25);
        r_minus = pow (pow (r_minus, 4) + pow (TP_epsilon, 4), 0.25);
        if (r_plus < TP_Tiny)
            r_plus = TP_Tiny;
        if (r_minus < TP_Tiny)
            r_minus = TP_Tiny;
        CCTK_REAL psi1 = 1
          + 0.5 * *mp / r_plus
          + 0.5 * *mm / r_minus + U.d0[0];
#define EXTEND(M,r) \
          ( M * (3./8 * pow(r, 4) / pow(TP_Extend_Radius, 5) - \
                 5./4 * pow(r, 2) / pow(TP_Extend_Radius, 3) + \
                 15./8 / TP_Extend_Radius))
        if (r_plus < TP_Extend_Radius) {
          psi1 = 1
             + 0.5 * EXTEND(*mp,r_plus)
             + 0.5 * *mm / r_minus + U.d0[0];
        }
        if (r_minus < TP_Extend_Radius) {
          psi1 = 1
             + 0.5 * EXTEND(*mm,r_minus)
             + 0.5 * *mp / r_plus + U.d0[0];
        }
        CCTK_REAL static_psi = 1;
        
        BY_Aijofxyz (x1, y1, z1, Aij);
        if (solve_momentum_constraint)
          for (int ii=1; ii<=3; ii++)
           for (int jj=1; jj<=3; jj++)
           {
             Aij[ii-1][jj-1] += tilde_A(ii, jj, U);
             /* This is only a test and can be removed once the code is
                working */
#if 0
             switch(ii){
               case 1: Aij[ii-1][jj-1] = U.d1[jj]; break;
               case 2: Aij[ii-1][jj-1] = U.d2[jj]; break;
               case 3: Aij[ii-1][jj-1] = U.d3[jj]; break;
             }
#endif
           }

        CCTK_REAL old_alp;
        if (multiply_old_lapse)
            old_alp = alp[ind];

        if ((*conformal_state > 0) || (pmn_lapse) || (brownsville_lapse)) {

          CCTK_REAL xp, yp, zp, rp, ir;
          CCTK_REAL s1, s3, s5;
          CCTK_REAL p, px, py, pz, pxx, pxy, pxz, pyy, pyz, pzz;
          p = 1.0;
          px = py = pz = 0.0;
          pxx = pxy = pxz = 0.0;
          pyy = pyz = pzz = 0.0;

          /* first puncture */
          xp = x1 - par_b;
          yp = y1;
          zp = z1;
          rp = sqrt (xp*xp + yp*yp + zp*zp);
          rp = pow (pow (rp, 4) + pow (TP_epsilon, 4), 0.25);
          if (rp < TP_Tiny)
              rp = TP_Tiny;
          ir = 1.0/rp;

          if (rp < TP_Extend_Radius) {
            ir = EXTEND(1., rp);
          }

          s1 = 0.5* *mp *ir;
          s3 = -s1*ir*ir;
          s5 = -3.0*s3*ir*ir;

          p += s1;

          px += xp*s3;
          py += yp*s3;
          pz += zp*s3;

          pxx += xp*xp*s5 + s3;
          pxy += xp*yp*s5;
          pxz += xp*zp*s5;
          pyy += yp*yp*s5 + s3;
          pyz += yp*zp*s5;
          pzz += zp*zp*s5 + s3;

          /* second puncture */
          xp = x1 + par_b;
          yp = y1;
          zp = z1;
          rp = sqrt (xp*xp + yp*yp + zp*zp);
          rp = pow (pow (rp, 4) + pow (TP_epsilon, 4), 0.25);
          if (rp < TP_Tiny)
              rp = TP_Tiny;
          ir = 1.0/rp;

          if (rp < TP_Extend_Radius) {
            ir = EXTEND(1., rp);
          }

          s1 = 0.5* *mm *ir;
          s3 = -s1*ir*ir;
          s5 = -3.0*s3*ir*ir;

          p += s1;

          px += xp*s3;
          py += yp*s3;
          pz += zp*s3;

          pxx += xp*xp*s5 + s3;
          pxy += xp*yp*s5;
          pxz += xp*zp*s5;
          pyy += yp*yp*s5 + s3;
          pyz += yp*zp*s5;
          pzz += zp*zp*s5 + s3;

          if (*conformal_state >= 1) {
            static_psi = p;
            psi[ind] = static_psi;
          }
          if (*conformal_state >= 2) {
            psix[ind] = px / static_psi;
            psiy[ind] = py / static_psi;
            psiz[ind] = pz / static_psi;
          }
          if (*conformal_state >= 3) {
            psixx[ind] = pxx / static_psi;
            psixy[ind] = pxy / static_psi;
            psixz[ind] = pxz / static_psi;
            psiyy[ind] = pyy / static_psi;
            psiyz[ind] = pyz / static_psi;
            psizz[ind] = pzz / static_psi;
          }

          if (pmn_lapse)
            alp[ind] = pow(p, initial_lapse_psi_exponent);
          if (brownsville_lapse)
            alp[ind] = 2.0/(1.0+pow(p, initial_lapse_psi_exponent));

        } /* if conformal-state > 0 */
          
        puncture_u[ind] = U.d0[0];

        gxx[ind] = pow (psi1 / static_psi, 4);
        gxy[ind] = 0;
        gxz[ind] = 0;
        gyy[ind] = pow (psi1 / static_psi, 4);
        gyz[ind] = 0;
        gzz[ind] = pow (psi1 / static_psi, 4);

        kxx[ind] = Aij[0][0] / pow(psi1, 2);
        kxy[ind] = Aij[0][1] / pow(psi1, 2);
        kxz[ind] = Aij[0][2] / pow(psi1, 2);
        kyy[ind] = Aij[1][1] / pow(psi1, 2);
        kyz[ind] = Aij[1][2] / pow(psi1, 2);
        kzz[ind] = Aij[2][2] / pow(psi1, 2);
        /* This is only a test */
#if 0
        kxx[ind] = Aij[0][0];
        kxy[ind] = Aij[0][1];
        kxz[ind] = Aij[0][2];
        kyy[ind] = Aij[1][1];
        kyz[ind] = Aij[1][2];
        kzz[ind] = Aij[2][2];
#endif

        if (antisymmetric_lapse || averaged_lapse) {
          alp[ind] =
            ((1.0 -0.5* *mp /r_plus -0.5* *mm/r_minus)
            /(1.0 +0.5* *mp /r_plus +0.5* *mm/r_minus));

          if (r_plus < TP_Extend_Radius) {
            alp[ind] =
              ((1.0 -0.5*EXTEND(*mp, r_plus) -0.5* *mm/r_minus)
              /(1.0 +0.5*EXTEND(*mp, r_plus) +0.5* *mm/r_minus));
          }
          if (r_minus < TP_Extend_Radius) {
            alp[ind] =
              ((1.0 -0.5*EXTEND(*mm, r_minus) -0.5* *mp/r_plus)
              /(1.0 +0.5*EXTEND(*mp, r_minus) +0.5* *mp/r_plus));
          }
          
          if (averaged_lapse) {
            alp[ind] = 0.5 * (1.0 + alp[ind]);
          }
        }
        if (multiply_old_lapse)
          alp[ind] *= old_alp;

        if (swap_xz) {
          /* Swap the x and z components of all tensors */
          if (*conformal_state >= 2) {
            SWAP (psix[ind], psiz[ind]);
          }
          if (*conformal_state >= 3) {
            SWAP (psixx[ind], psizz[ind]);
            SWAP (psixy[ind], psiyz[ind]);
          }
          SWAP (gxx[ind], gzz[ind]);
          SWAP (gxy[ind], gyz[ind]);
          SWAP (kxx[ind], kzz[ind]);
          SWAP (kxy[ind], kyz[ind]);
        } /* if swap_xz */

      } /* for i */
    }   /* for j */
  }     /* for k */
  free_derivs (&U, nvar);
  }
  if (do_misc_debug_output)
    fclose(debugfile);

  if (use_sources && rescale_sources)
  {
    Rescale_Sources(cctkGH,
                    cctk_lsh[0]*cctk_lsh[1]*cctk_lsh[2],
                    x, y, z,
                    (*conformal_state > 0) ? psi : NULL,
                    gxx, gyy, gzz,
                    gxy, gxz, gyz);
  }

  if (0) {
    /* Keep the result around for the next time */
    free_dvector (F, 0, ntotal - 1);
    free_derivs (&u, ntotal);
    free_derivs (&v, ntotal);
  }
  if ((gsm == GSM_Taylor_expansion || gsm == GSM_Mixed) && solve_momentum_constraint)
  {
    free_derivs (&aij, n1*n2*n3*6);
    free_derivs (&v3, ntotal);
    free_derivs (&v2, ntotal);
    free_derivs (&v1, ntotal);
  }

  PunctEvalAtArbitPositionU();
  chebft_Zeros_free_caches();
  fourft_free_caches();
}

