/* TwoPunctures:  File  "TwoPunctures.h"*/

#define StencilSize 19
#define N_PlaneRelax 1
#define NRELAX 200
#define Step_Relax 1

typedef struct DERIVS
{
  CCTK_REAL *d0, *d1, *d2, *d3, *d11, *d12, *d13, *d22, *d23, *d33;
} derivs;

/*
Files of "TwoPunctures":
	TwoPunctures.c
	FuncAndJacobian.c
	CoordTransf.c
	Equations.c
	Newton.c
	utilities.c (see utilities.h)
**************************
*/

/* Routines in  "TwoPunctures.c"*/
CCTK_REAL TestSolution (CCTK_REAL A, CCTK_REAL B, CCTK_REAL X, CCTK_REAL R, CCTK_REAL phi);
void TestVector_w (CCTK_REAL *par, int nvar, int n1, int n2, int n3, CCTK_REAL *w);

/* Routines in  "FuncAndJacobian.c"*/
int Index (int ivar, int i, int j, int k, int nvar, int n1, int n2, int n3);
void allocate_derivs (derivs * v, int n);
void free_derivs (derivs * v, int n);
void Derivatives_AB3 (int nvar, int n1, int n2, int n3, derivs v);
void F_of_v (CCTK_POINTER_TO_CONST cctkGH,
       int nvar, int n1, int n2, int n3, derivs v,
	     CCTK_REAL *F, derivs u, CCTK_REAL *sources);
void J_times_dv (int nvar, int n1, int n2, int n3, derivs dv,
		 CCTK_REAL *Jdv, derivs u, CCTK_REAL *sources);
void JFD_times_dv (int i, int j, int k, int nvar, int n1,
		   int n2, int n3, CCTK_INT dv_ind, derivs u, CCTK_REAL *values,
       CCTK_REAL *sources);
void SetMatrix_JFD (int nvar, int n1, int n2, int n3,
		    derivs u, int *ncols, int **cols, CCTK_REAL **Matrix,
        CCTK_REAL *sources);
CCTK_REAL PunctEvalAtArbitPosition (int ivar, int nvar,
                                    CCTK_REAL *v, CCTK_REAL A, CCTK_REAL B, CCTK_REAL phi,
				 int n1, int n2, int n3);
CCTK_REAL PunctEvalCoeffsAtArbitPosition (CCTK_REAL *v, int ivar, CCTK_REAL A, CCTK_REAL B, CCTK_REAL phi,
                                 int nvar, int n1, int n2, int n3);
void      PunctEvalAtArbitPositionI (int nvar, CCTK_REAL *v, int n1, int n2, int n3, int buffer);
void      PunctEvalAtArbitPositionU (void);
CCTK_REAL PunctEvalAtArbitPositionF (int ivar, int nvar,
                                    CCTK_REAL *v, CCTK_REAL A, CCTK_REAL B, CCTK_REAL phi,
				 int n1, int n2, int n3, int buffer);
void calculate_derivs (int i, int j, int k, int ivar, int nvar, int n1,
		       int n2, int n3, derivs v, derivs vv);
CCTK_REAL interpol (CCTK_REAL a, CCTK_REAL b, CCTK_REAL c, derivs v);
CCTK_REAL PunctTaylorExpandAtArbitPosition (int ivar, int nvar, int n1,
                                         int n2, int n3, derivs v, CCTK_REAL x,
                                         CCTK_REAL y, CCTK_REAL z,
                                         const int Am1);
CCTK_REAL PunctFDOAtArbitPosition           (int ivar, int nvar, int n1,
                                         int n2, int n3, CCTK_REAL *v, CCTK_REAL x,
                                         CCTK_REAL y, CCTK_REAL z,
                                         const int Am1);
void       PunctFDAtArbitPosition           (derivs U, int ivar, int nvar, int n1,
                                         int n2, int n3, derivs v, CCTK_REAL x,
                                         CCTK_REAL y, CCTK_REAL z, CCTK_REAL h[3],
                                         const int Am1);
void       PunctFDAtArbitPositionEval       (derivs U, int ivar, int nvar, int n1,
                                         int n2, int n3, derivs v, CCTK_REAL x,
                                         CCTK_REAL y, CCTK_REAL z, CCTK_REAL h[3],
                                         const int Am1);
CCTK_REAL PunctIntPolAtArbitPosition (int ivar, int nvar, int n1,
				   int n2, int n3, double *v, CCTK_REAL x,
				   CCTK_REAL y, CCTK_REAL z, const int Am1);
CCTK_REAL PunctIntPolAtArbitPositionF (int ivar, int nvar, int n1,
				   int n2, int n3, double *v, CCTK_REAL x,
				   CCTK_REAL y, CCTK_REAL z, const int Am1, int buffer);
CCTK_REAL PunctIntPolCoeffsAtArbitPosition (int ivar, int nvar, int n1,
				   int n2, int n3, double *v, CCTK_REAL x,
				   CCTK_REAL y, CCTK_REAL z, int Am1);

/* Routines in  "CoordTransf.c"*/
void AB_To_XR (int nvar, CCTK_REAL A, CCTK_REAL B, CCTK_REAL *X,
	       CCTK_REAL *R, derivs U);
void C_To_c (int nvar, CCTK_REAL X, CCTK_REAL R, CCTK_REAL *x,
	     CCTK_REAL *r, derivs U);
void rx3_To_xyz (int nvar, CCTK_REAL x, CCTK_REAL r, CCTK_REAL phi, CCTK_REAL *y,
		 CCTK_REAL *z, derivs U);
void save_C_To_c (int nvar, CCTK_REAL X, CCTK_REAL R, CCTK_REAL *x,
	     CCTK_REAL *r, derivs U);
void save_rx3_To_xyz (int nvar, CCTK_REAL x, CCTK_REAL r, CCTK_REAL phi, CCTK_REAL *y,
		 CCTK_REAL *z, derivs U);

/* Routines in  "Equations.c"*/
CCTK_REAL BY_KKofxyz (CCTK_REAL x, CCTK_REAL y, CCTK_REAL z);
void BY_Aijofxyz (CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL Aij[3][3]);
void NonLinEquations (int nvar, CCTK_REAL *sources,
          CCTK_REAL A, CCTK_REAL B, CCTK_REAL X, CCTK_REAL R,
		      CCTK_REAL x, CCTK_REAL r, CCTK_REAL phi,
		      CCTK_REAL y, CCTK_REAL z, derivs U, CCTK_REAL *values);
void LinEquations (int nvar, CCTK_REAL *sources,
          CCTK_REAL A, CCTK_REAL B, CCTK_REAL X, CCTK_REAL R,
		      CCTK_REAL x, CCTK_REAL r, CCTK_REAL phi,
		      CCTK_REAL y, CCTK_REAL z, derivs dU, derivs U, CCTK_REAL *values);

/* Routines in  "Newton.c"*/
void TestRelax (CCTK_POINTER_TO_CONST cctkGH,
                int nvar, int n1, int n2, int n3, derivs v, CCTK_REAL *dv,
                CCTK_REAL *sources);
void Newton (CCTK_POINTER_TO_CONST cctkGH,
             int nvar, int n1, int n2, int n3, derivs v,
	           CCTK_REAL tol, int itmax, CCTK_REAL *sources);


/* 
 27: -1.325691774825335e-03
 37: -1.325691778944117e-03
 47: -1.325691778942711e-03
 
 17: -1.510625972641537e-03
 21: -1.511443006977708e-03
 27: -1.511440785153687e-03
 37: -1.511440809549005e-03
 39: -1.511440809597588e-03
 */
static inline CCTK_REAL tilde_A(const int i, const int j, const derivs U)
{
  CCTK_REAL ret = 0.0;
  switch(i)
  {
    case 1: ret += U.d1[j]; break;
    case 2: ret += U.d2[j]; break;
    case 3: ret += U.d3[j]; break;
  }
  switch(j)
  {
    case 1: ret += U.d1[i]; break;
    case 2: ret += U.d2[i]; break;
    case 3: ret += U.d3[i]; break;
  }
  if (i==j)
     ret -= 2./3 * (U.d1[1] + U.d2[2] + U.d3[3]);
  return ret;
}

static inline CCTK_REAL tilde_A_2(const int i, const int j, const derivs U)
{
  CCTK_REAL ret = 0.0;
  switch(i)
  {
    case 1: ret += U.d1[j]; break;
    case 2: ret += U.d2[j]; break;
    case 3: ret += U.d3[j]; break;
  }
  switch(j)
  {
    case 1: ret += U.d1[i]; break;
    case 2: ret += U.d2[i]; break;
    case 3: ret += U.d3[i]; break;
  }
  if (i==j)
     ret -= 2./3 * U.d11[0];
  return ret;
}

static inline CCTK_REAL tilde_A_tilde_A(const derivs U)
{
  CCTK_REAL ret = 0.0, tmp;
  for (int i=1; i<4; i++)
    for (int j=1; j<4; j++)
    {
      tmp = tilde_A(i,j,U);
      ret += tmp*tmp;
    }
  return ret;
}

static inline CCTK_REAL min (CCTK_REAL const x, CCTK_REAL const y)
{
  return x<y ? x : y;
}

